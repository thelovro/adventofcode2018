﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Day23
{
    class Program : BaseAoc
    {
        static void Main(string[] args)
        {
            Program program = new Program();
            program.SampleInputPartOne.Add(@"pos=<0,0,0>, r=4
pos=<1,0,0>, r=1
pos=<4,0,0>, r=3
pos=<0,2,0>, r=1
pos=<0,5,0>, r=3
pos=<0,0,3>, r=1
pos=<1,1,1>, r=1
pos=<1,1,2>, r=1
pos=<1,3,1>, r=1", "7");

            program.SampleInputPartTwo.Add(@"pos=<10,12,12>, r=2
pos=<12,14,12>, r=2
pos=<16,12,12>, r=4
pos=<14,14,14>, r=6
pos=<50,50,50>, r=200
pos=<10,10,10>, r=5", "36");

            program.Solve();
        }

        protected override string FindFirstSolution(string[] input)
        {
            List<NanoBot> bots = ParseInput(input);
            NanoBot bot = bots.First(b => b.Range == bots.Max(m => m.Range));

            int numberOfBotsInRange = GetNumberOfBotsInRange(bot, bots);

            return numberOfBotsInRange.ToString();
        }

        protected override string FindSecondSolution(string[] input)
        {
            List<NanoBot> bots = ParseInput(input);
            var position = GetMagicPosition2(bots);
            int soluton = GetManhattanForPosition(position);

            return soluton.ToString();
        }

        private int GetNumberOfBotsInRange(NanoBot botWithMaxRange, List<NanoBot> bots)
        {
            int counter = 0;

            foreach (var bot in bots)
            {
                if (Manhattan(botWithMaxRange.Position, bot.Position) <= botWithMaxRange.Range)
                {
                    counter++;
                }
            }

            return counter;
        }

        private List<NanoBot> ParseInput(string[] input)
        {
            List<NanoBot> bots = new List<NanoBot>();
            foreach (string row in input)
            {
                string[] parts = row.Split(", ");
                string position = parts[0].Replace("pos=", "").TrimStart('<').TrimEnd('>');
                bots.Add(new NanoBot() { X = int.Parse(position.Split(",")[0]), Y = int.Parse(position.Split(",")[1]), Z = int.Parse(position.Split(",")[2]), Range = int.Parse(parts[1].Replace("r=", "")) });
            }

            return bots;
        }

        private int Manhattan((int, int, int) firstBot, (int, int, int) secondBot)
        {
            return Math.Abs(firstBot.Item1 - secondBot.Item1) + Math.Abs(firstBot.Item2 - secondBot.Item2) + Math.Abs(firstBot.Item3 - secondBot.Item3);
        }

        private int GetManhattanForPosition((int, int, int) position)
        {
            return position.Item1 + position.Item2 + position.Item3;
        }

        private (int, int, int) GetMagicPosition2(List<NanoBot> bots)
        {
            int minX = bots.Min(b => b.X);
            int maxX = bots.Max(b => b.X);
            int minY = bots.Min(b => b.Y);
            int maxY = bots.Max(b => b.Y);
            int minZ = bots.Min(b => b.Z);
            int maxZ = bots.Max(b => b.Z);

            (int, int, int) position = (0, 0, 0);
            int numberOfBotsInRange = 0;

            for (int factor = 100000000; factor >= 1; factor = factor / 10)
            {
                for (int x = minX; x < maxX; x += factor)
                {
                    for (int y = minY; y < maxY; y += factor)
                    {
                        for (int z = minZ; z < maxZ; z += factor)
                        {
                            int counter = 0;
                            foreach (var bot in bots)
                            {
                                if (Manhattan(bot.Position, (x, y, z)) <= bot.Range)
                                {
                                    counter++;
                                }
                            }

                            if (counter > numberOfBotsInRange)
                            {
                                numberOfBotsInRange = counter;
                                position = (x, y, z);
                            }
                            else if (counter == numberOfBotsInRange && GetManhattanForPosition((x, y, z)) < GetManhattanForPosition(position))
                            {
                                position = (x, y, z);
                            }
                        }
                    }
                }

                minX = position.Item1 - factor;
                maxX = position.Item1 + factor;
                minY = position.Item2 - factor;
                maxY = position.Item2 + factor;
                minZ = position.Item3 - factor;
                maxZ = position.Item3 + factor;
            }

            return position;
        }
    }

    public class NanoBot
    {
        public int X;
        public int Y;
        public int Z;
        public (int, int, int) Position { get { return (X, Y, Z); } }
        public int Range;
    }
}