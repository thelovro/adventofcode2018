﻿using System;
using System.Linq;
using System.Collections.Generic;
using Common;

namespace Day07
{
    class Program : BaseAoc
    {
        static void Main(string[] args)
        {
            Program program = new Program();
            program.SampleInputPartOne.Add(@"Step C must be finished before step A can begin.
Step C must be finished before step F can begin.
Step A must be finished before step B can begin.
Step A must be finished before step D can begin.
Step B must be finished before step E can begin.
Step D must be finished before step E can begin.
Step F must be finished before step E can begin.", "CABDFE");

            program.SampleInputPartTwo.Add(@"Step C must be finished before step A can begin.
Step C must be finished before step F can begin.
Step A must be finished before step B can begin.
Step A must be finished before step D can begin.
Step B must be finished before step E can begin.
Step D must be finished before step E can begin.
Step F must be finished before step E can begin.", "15");

            program.Solve();
        }

        protected override string FindFirstSolution(string[] input)
        {
            Dictionary<string, List<string>> steps = new Dictionary<string, List<string>>();
            List<string> distinctLetters = new List<string>();
            ParseInput(input, out steps, out distinctLetters);

            var length = steps.Select(x => x.Key).Distinct().Count();
            List<string> solution = new List<string>();

            var orderedSteps = steps.OrderBy(x => x.Value.Count).ThenBy(x => x.Key);
            //find result
            while (solution.Count() < length)
            {
                foreach (var step in orderedSteps)
                {
                    foreach (string sol in solution)
                    {
                        if (step.Value.Contains(sol))
                        {
                            step.Value.Remove(sol);
                        }
                    }
                }
                orderedSteps = orderedSteps.OrderBy(x => x.Value.Count).ThenBy(x => x.Key);
                var nextStep = orderedSteps.First();
                solution.Add(nextStep.Key);
                steps.Remove(nextStep.Key);
            }

            return string.Join("", solution);
        }

        protected override string FindSecondSolution(string[] input)
        {
            Dictionary<string, List<string>> steps;
            List<string> distinctLetters;
            ParseInput(input, out steps, out distinctLetters);

            var workingItems = new int[26];
            for (int i = 0; i < workingItems.Length; i++)
            {
                workingItems[i] = int.MaxValue;
            }

            List<string> solution = new List<string>();
            int counter = 0;
            int numberOfWorkers = IsTest ? 2 : 5;
            int[] workerTimes = new int[numberOfWorkers];
            foreach (int i in workerTimes)
            {
                workerTimes[i] = 0;
            }

            while (steps.Count() > 0 || workerTimes.Any(x => x > 0))
            {
                for (int i = 0; i < workerTimes.Length; i++)
                {
                    if (workerTimes[i] > 0)
                    {
                        workerTimes[i] -= 1;
                    }
                }
                for (int i = 0; i < workingItems.Length; i++)
                {
                    workingItems[i]--;
                    if (workingItems[i] == 0)
                    {
                        solution.Add(distinctLetters[i]);
                    }
                }

                foreach (var step in steps)
                {
                    foreach (string sol in solution)
                    {
                        if (step.Value.Contains(sol))
                        {
                            step.Value.Remove(sol);
                        }
                    }
                }

                int additionalTime = IsTest ? 0 : 60;
                //do work among workers
                if (steps.Count() > 0 && workerTimes.Any(x => x == 0))
                {
                    for (int i = 0; i < workerTimes.Length; i++)
                    {
                        if (workerTimes[i] == 0)
                        {
                            var orderedSteps = steps.Where(x => x.Value.Count() == 0).OrderBy(x => x.Value.Count).ThenBy(x => x.Key);
                            if (orderedSteps.Any())
                            {
                                var nextStep = orderedSteps.First();

                                steps.Remove(nextStep.Key);
                                //attach time to letter and worker
                                workingItems[distinctLetters.IndexOf(nextStep.Key)] = distinctLetters.IndexOf(nextStep.Key) + 1 + additionalTime;
                                workerTimes[i] = distinctLetters.IndexOf(nextStep.Key) + 1 + additionalTime;
                            }
                        }
                    }
                }
                //count time
                counter++;
            }
            counter--;
            return counter.ToString();
        }

        private static void ParseInput(string[] input, out Dictionary<string, List<string>> steps, out List<string> distinctLetters)
        {
            steps = new Dictionary<string, List<string>>();
            distinctLetters = new List<string>();
            foreach (string row in input)
            {
                input = row.Split(" ");
                string condition = input[1];
                string step = input[7];
                if (!distinctLetters.Contains(condition))
                {
                    distinctLetters.Add(condition);
                }
                if (!distinctLetters.Contains(step))
                {
                    distinctLetters.Add(step);
                }

                if (steps.ContainsKey(step))
                {
                    if (steps[step].Contains(condition))
                    {
                        continue;
                    }
                    else
                    {
                        steps[step].Add(condition);
                    }
                }
                else
                {
                    steps.Add(step, new List<string>() { condition });
                }
            }

            distinctLetters.Sort();
            foreach (var letter in distinctLetters)
            {
                if (!steps.ContainsKey(letter))
                {
                    steps.Add(letter, new List<string>());
                }
            }
        }
    }
}