﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Day14
{
    class Program : BaseAoc
    {
        static void Main(string[] args)
        {
            Program program = new Program();
            program.SampleInputPartOne.Add(@"9", "5158916779");
            program.SampleInputPartOne.Add(@"5", "0124515891");
            program.SampleInputPartOne.Add(@"18", "9251071085");
            program.SampleInputPartOne.Add(@"2018", "5941429882");

            program.SampleInputPartTwo.Add(@"51589", "9");
            program.SampleInputPartTwo.Add(@"01245", "5");
            program.SampleInputPartTwo.Add(@"92510", "18");
            program.SampleInputPartTwo.Add(@"59414", "2018");

            program.Solve();
        }

        protected override string FindFirstSolution(string[] input)
        {
            int limit = int.Parse(input[0]);

            List<int> scoreboard = new List<int>();
            scoreboard.Add(3);
            scoreboard.Add(7);

            int currentElfOneIndex = 0;
            int currentElfTwoIndex = 1;

            while (scoreboard.Count() - 10 < limit)
            {
                int newRecipeScore = scoreboard[currentElfOneIndex] + scoreboard[currentElfTwoIndex];
                foreach (char c in newRecipeScore.ToString())
                {
                    scoreboard.Add(int.Parse(c.ToString()));
                }

                currentElfOneIndex = (currentElfOneIndex + 1 + scoreboard[currentElfOneIndex]) % scoreboard.Count();
                currentElfTwoIndex = (currentElfTwoIndex + 1 + scoreboard[currentElfTwoIndex]) % scoreboard.Count();
            }

            string solution = string.Empty;
            for (int i = limit; i < limit + 10; i++)
            {
                solution += scoreboard[i].ToString();
            }

            return solution;
        }

        protected override string FindSecondSolution(string[] input)
        {
            string finalSequence = input[0];

            List<int> scoreboard = new List<int>();
            scoreboard.Add(3);
            scoreboard.Add(7);

            int currentElfOneIndex = 0;
            int currentElfTwoIndex = 1;

            while (true)
            {
                int newRecipeScore = scoreboard[currentElfOneIndex] + scoreboard[currentElfTwoIndex];
                foreach (char c in newRecipeScore.ToString())
                {
                    scoreboard.Add(int.Parse(c.ToString()));

                    if (scoreboard.Count >= finalSequence.Length)
                    {
                        string sequence = "";
                        for (int i = scoreboard.Count() - finalSequence.Length; i < scoreboard.Count(); i++)
                        {
                            sequence += scoreboard[i].ToString();
                        }

                        if (sequence == finalSequence)
                        {
                            return (scoreboard.Count() - finalSequence.Length).ToString();
                        }
                    }
                }

                currentElfOneIndex = (currentElfOneIndex + 1 + scoreboard[currentElfOneIndex]) % scoreboard.Count();
                currentElfTwoIndex = (currentElfTwoIndex + 1 + scoreboard[currentElfTwoIndex]) % scoreboard.Count();
            }
        }
    }
}