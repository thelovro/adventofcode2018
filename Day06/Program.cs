﻿using Common;
using System;
using System.Linq;
using System.Collections.Generic;

namespace Day06
{
    class Program : BaseAoc
    {
        static void Main(string[] args)
        {
            Program program = new Program();
            program.SampleInputPartOne.Add(@"1, 1
1, 6
8, 3
3, 4
5, 5
8, 9", "17");

            program.SampleInputPartTwo.Add(@"1, 1
1, 6
8, 3
3, 4
5, 5
8, 9", "16");

            program.Solve();
        }

        protected override string FindFirstSolution(string[] input)
        {
            int maxValue = input.Max(x => x.Split(", ").Max(y => int.Parse(y)));
            int areaSize = maxValue + 1;
            string[,] area = new string[areaSize, areaSize];

            Dictionary<int, int[]> pointsCoordinates = new Dictionary<int, int[]>();
            for (int i = 0; i < input.Length; i++)
            {
                pointsCoordinates.Add(i, input[i].Split(", ").Select(x => int.Parse(x)).Append(0).ToArray());
            }

            int closestPoint = 0;
            int minDistance = areaSize * 10;
            bool multiplePoints = false;
            for (int i = 0; i < areaSize; i++)
            {
                for (int j = 0; j < areaSize; j++)
                {
                    foreach (var point in pointsCoordinates)
                    {
                        int manhattan = Manhattan(i, j, point.Value[0], point.Value[1]);
                        if (manhattan == minDistance)
                        {
                            multiplePoints = true;
                        }
                        else if (manhattan < minDistance)
                        {
                            minDistance = manhattan;
                            closestPoint = point.Key;
                            multiplePoints = false;
                        }
                    }

                    if (multiplePoints)
                    {
                        area[i, j] = ".";
                    }
                    else
                    {
                        area[i, j] = closestPoint.ToString();
                        pointsCoordinates[closestPoint][2]++;
                    }

                    //reset the items
                    closestPoint = 0;
                    minDistance = areaSize * 10;
                    multiplePoints = false;
                }
            }


            //check whether areas are infinite
            //we increase areasize on each side by 1 and repeat the process. If area size increases, it is infinite!
            //reset the items
            closestPoint = 0;
            minDistance = areaSize * 10;
            multiplePoints = false;
            for (int i = 0; i < areaSize + 2; i++)
            {
                for (int j = 0; j < areaSize + 2; j++)
                {
                    if (i == 0 || i == areaSize + 1 || j == 0 || j == areaSize + 1)
                    {
                        foreach (var point in pointsCoordinates)
                        {
                            int manhattan = Manhattan(i, j, point.Value[0] + 1, point.Value[1] + 1);
                            if (manhattan == minDistance)
                            {
                                multiplePoints = true;
                            }
                            else if (manhattan < minDistance)
                            {
                                minDistance = manhattan;
                                closestPoint = point.Key;
                                multiplePoints = false;
                            }
                        }

                        if (multiplePoints)
                        {

                        }
                        else
                        {
                            //if area size would increase it means that area is infinite - we set it to 0 so we don't count it at the end!
                            pointsCoordinates[closestPoint][2] = 0;
                        }

                        //reset the items
                        closestPoint = 0;
                        minDistance = areaSize * 10;
                        multiplePoints = false;
                    }
                }
            }

            return pointsCoordinates.Max(x => x.Value[2]).ToString();
        }

        private int Manhattan(int x1, int y1, int x2, int y2)
        {
            return Math.Abs(x1 - x2) + Math.Abs(y1 - y2);
        }

        protected override string FindSecondSolution(string[] input)
        {
            int maxValue = input.Max(x => x.Split(", ").Max(y => int.Parse(y)));
            int areaSize = maxValue + 1;
            int[,] area = new int[areaSize, areaSize];

            Dictionary<int, int[]> pointsCoordinates = new Dictionary<int, int[]>();
            for (int i = 0; i < input.Length; i++)
            {
                pointsCoordinates.Add(i, input[i].Split(", ").Select(x => int.Parse(x)).Append(0).ToArray());
            }

            int numberOfRegions = 0;
            for (int i = 0; i < areaSize; i++)
            {
                for (int j = 0; j < areaSize; j++)
                {
                    int sum = 0;
                    foreach (var point in pointsCoordinates)
                    {
                        int manhattan = Manhattan(i, j, point.Value[0], point.Value[1]);

                        sum += manhattan;
                    }

                    int limit = IsTest ? 32 : 10000;
                    if (sum < limit)
                    {
                        area[i, j] = -1;
                        numberOfRegions++;
                    }
                }
            }

            return numberOfRegions.ToString();
        }
    }
}