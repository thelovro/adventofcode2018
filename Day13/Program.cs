﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Day13
{
    class Program : BaseAoc
    {
        static void Main(string[] args)
        {
            Program program = new Program();
            program.SampleInputPartOne.Add(@"/->-\        
|   |  /----\
| /-+--+-\  |
| | |  | v  |
\-+-/  \-+--/
  \------/   ", "7,3");

            program.SampleInputPartTwo.Add(@"/>-<\  
|   |  
| /<+-\
| | | v
\>+</ |
  |   ^
  \<->/", "6,4");

            program.Solve();
        }

        enum Directions
        {
            Right,
            Down,
            Left,
            Up
        }

        enum Choices
        {
            Left,
            Straight,
            Right
        }

        protected override string FindFirstSolution(string[] input)
        {
            Dictionary<int, Cart> carts;
            string[,] map;
            PrepareData(input, out carts, out map);
            string solution = string.Empty;

            int ticks = 0;
            //let's go!
            while (true)
            {
                foreach (var cart in carts.OrderBy(x => x.Value.PositionY).ThenBy(x => x.Value.PositionX))
                {
                    switch (cart.Value.CurrentDirection)
                    {
                        case Directions.Down:
                            {
                                cart.Value.CurrentDirection = GetDirection(map, cart.Value, cart.Value.PositionX, cart.Value.PositionY + 1);
                                cart.Value.PositionY += 1;
                                break;
                            }
                        case Directions.Left:
                            {
                                cart.Value.CurrentDirection = GetDirection(map, cart.Value, cart.Value.PositionX - 1, cart.Value.PositionY);
                                cart.Value.PositionX -= 1;
                                break;
                            }
                        case Directions.Up:
                            {
                                cart.Value.CurrentDirection = GetDirection(map, cart.Value, cart.Value.PositionX, cart.Value.PositionY - 1);
                                cart.Value.PositionY -= 1;
                                break;
                            }
                        case Directions.Right:
                            {
                                cart.Value.CurrentDirection = GetDirection(map, cart.Value, cart.Value.PositionX + 1, cart.Value.PositionY);
                                cart.Value.PositionX += 1;
                                break;
                            }
                        default:
                            throw new Exception("bleh");
                    }

                    if (carts.GroupBy(x => new { x.Value.PositionX, x.Value.PositionY }).Any(x => x.Count() > 1))
                    {
                        //we have collission
                        var a = carts.GroupBy(x => new { x.Value.PositionX, x.Value.PositionY }).Where(x => x.Count() > 1).Select(x => new { x.Key.PositionX, x.Key.PositionY });
                        solution = string.Format("{0},{1}", a.First().PositionX, a.First().PositionY);
                        break;
                    }
                }
                ticks++;

                if (solution.Length > 0)
                {
                    break;
                }
            }

            return solution;
        }

        protected override string FindSecondSolution(string[] input)
        {
            Dictionary<int, Cart> carts;
            string[,] map;
            PrepareData(input, out carts, out map);
            string solution = string.Empty;

            List<int> cartsToRemove = new List<int>();

            int ticks = 0;
            //let's go!
            while (true)
            {
                foreach (var cart in carts.OrderBy(x => x.Value.PositionY).ThenBy(x => x.Value.PositionX))
                {
                    switch (cart.Value.CurrentDirection)
                    {
                        case Directions.Down:
                            {
                                cart.Value.CurrentDirection = GetDirection(map, cart.Value, cart.Value.PositionX, cart.Value.PositionY + 1);
                                cart.Value.PositionY += 1;
                                break;
                            }
                        case Directions.Left:
                            {
                                cart.Value.CurrentDirection = GetDirection(map, cart.Value, cart.Value.PositionX - 1, cart.Value.PositionY);
                                cart.Value.PositionX -= 1;
                                break;
                            }
                        case Directions.Up:
                            {
                                cart.Value.CurrentDirection = GetDirection(map, cart.Value, cart.Value.PositionX, cart.Value.PositionY - 1);
                                cart.Value.PositionY -= 1;
                                break;
                            }
                        case Directions.Right:
                            {
                                cart.Value.CurrentDirection = GetDirection(map, cart.Value, cart.Value.PositionX + 1, cart.Value.PositionY);
                                cart.Value.PositionX += 1;
                                break;
                            }
                        default:
                            throw new Exception("bleh");
                    }

                    if (carts.Where(x => !cartsToRemove.Contains(x.Key)).GroupBy(x => new { x.Value.PositionX, x.Value.PositionY }).Any(x => x.Count() > 1))
                    {
                        //we have collission
                        var a = carts.Where(x => !cartsToRemove.Contains(x.Key))
                            .GroupBy(x => new { x.Value.PositionX, x.Value.PositionY })
                            .Where(x => x.Count() > 1)
                            .Select(x => new { x.Key.PositionX, x.Key.PositionY });
                        int posX = a.First().PositionX;
                        int posY = a.First().PositionY;

                        foreach (var cartNumber in carts.Where(x => x.Value.PositionX == posX && x.Value.PositionY == posY).Select(x => x.Key))
                        {
                            cartsToRemove.Add(cartNumber);
                        }
                    }
                }

                foreach (var cartNumber in cartsToRemove)
                {
                    carts.Remove(cartNumber);
                }
                cartsToRemove = new List<int>();
                ticks++;

                if (carts.Count() == 1)
                {
                    solution = string.Format("{0},{1}", carts.First().Value.PositionX, carts.First().Value.PositionY);
                    break;
                }
            }

            return solution;
        }

        private void PrepareData(string[] input, out Dictionary<int, Cart> carts, out string[,] map)
        {
            int mapSizeX = input.Max(x => x.Length);
            int mapSizeY = input.Length;
            int cartCounter = 0;
            carts = new Dictionary<int, Cart>();
            //prepare data
            map = new string[mapSizeY, mapSizeX];
            for (int i = 0; i < mapSizeY; i++)
            {
                for (int j = 0; j < mapSizeX; j++)
                {
                    string character = input[i][j].ToString();

                    if (character == "<" || character == ">" || character == "v" || character == "^")
                    {
                        var cart = new Cart();
                        cart.CurrentDirection = cart.GetCurrentDirection(character);
                        cart.PositionX = j;
                        cart.PositionY = i;
                        cart.SerialNumber = cartCounter;
                        carts.Add(cartCounter++, cart);

                        //fix the map:
                        switch (character)
                        {
                            case "<":
                            case ">":
                                character = "-";
                                break;
                            case "v":
                            case "^":
                                character = "|";
                                break;
                        }
                    }
                    map[i, j] = character;
                }
            }
        }

        private Directions GetDirection(string[,] map, Cart cart, int newPositionX, int newPositionY)
        {
            string newMapSign = map[newPositionY, newPositionX];

            if (cart.PositionX > newPositionX)
            {
                //left
                switch (newMapSign)
                {
                    case "+":
                        switch (cart.NextTurn)
                        {
                            case Choices.Left:
                                return Directions.Down;
                            case Choices.Right:
                                return Directions.Up;
                            case Choices.Straight:
                                return Directions.Left;
                            default:
                                throw new Exception("aloha");
                        }
                    case "/":
                        return Directions.Down;
                    case @"\":
                        return Directions.Up;
                    case "-":
                        return Directions.Left;
                    case "|":
                    default:
                        throw new Exception("aloha");
                }
            }
            else if (cart.PositionX < newPositionX)
            {
                //right
                switch (newMapSign)
                {
                    case "+":
                        switch (cart.NextTurn)
                        {
                            case Choices.Left:
                                return Directions.Up;
                            case Choices.Right:
                                return Directions.Down;
                            case Choices.Straight:
                                return Directions.Right;
                            default:
                                throw new Exception("aloha");
                        }
                    case "/":
                        return Directions.Up;
                    case @"\":
                        return Directions.Down;
                    case "-":
                        return Directions.Right;
                    case "|":
                    default:
                        throw new Exception("aloha");
                }
            }
            else if (cart.PositionY > newPositionY)
            {
                //up
                switch (newMapSign)
                {
                    case "+":
                        switch (cart.NextTurn)
                        {
                            case Choices.Left:
                                return Directions.Left;
                            case Choices.Right:
                                return Directions.Right;
                            case Choices.Straight:
                                return Directions.Up;
                            default:
                                throw new Exception("aloha");
                        }
                    case "/":
                        return Directions.Right;
                    case @"\":
                        return Directions.Left;
                    case "-":
                        throw new Exception("aloha");
                    case "|":
                        return Directions.Up;
                    default:
                        throw new Exception("aloha");
                }
            }
            else if (cart.PositionY < newPositionY)
            {
                //down
                switch (newMapSign)
                {
                    case "+":
                        switch (cart.NextTurn)
                        {
                            case Choices.Left:
                                return Directions.Right;
                            case Choices.Right:
                                return Directions.Left;
                            case Choices.Straight:
                                return Directions.Down;
                            default:
                                throw new Exception("aloha");
                        }
                    case "/":
                        return Directions.Left;
                    case @"\":
                        return Directions.Right;
                    case "-":
                        throw new Exception("aloha");
                    case "|":
                        return Directions.Down;
                    default:
                        throw new Exception("aloha");
                }
            }

            throw new Exception("aloha");
        }

        class Cart
        {
            public int SerialNumber { get; set; }
            public Directions CurrentDirection { get; set; }
            Choices _nextTurn = Choices.Left;
            public int PositionX { get; set; }
            public int PositionY { get; set; }
            public Choices NextTurn
            {
                get
                {
                    Choices tmp = _nextTurn;
                    if (_nextTurn == Choices.Left)
                        _nextTurn = Choices.Straight;
                    else if (_nextTurn == Choices.Straight)
                        _nextTurn = Choices.Right;
                    else if (_nextTurn == Choices.Right)
                        _nextTurn = Choices.Left;

                    return tmp;
                }
            }
            public Directions GetCurrentDirection(string input)
            {
                switch (input)
                {
                    case "<":
                        return Directions.Left;
                    case ">":
                        return Directions.Right;
                    case "v":
                        return Directions.Down;
                    case "^":
                        return Directions.Up;
                    default:
                        throw new Exception("this should not happend!");
                }
            }
        }
    }
}