﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Day19
{
    class Program : BaseAoc
    {
        static void Main(string[] args)
        {
            Program program = new Program();
            program.SampleInputPartOne.Add(@"#ip 0
seti 5 0 1
seti 6 0 2
addi 0 1 0
addr 1 2 3
setr 1 0 0
seti 8 0 4
seti 9 0 5", "6");

            program.Solve();
        }

        protected override string FindFirstSolution(string[] input)
        {
            int[] register = new int[] { 0, 0, 0, 0, 0, 0 };
            int ipIndex = int.Parse(input[0].Replace("#ip ", ""));
            Dictionary<int, string> operations = GetOperationsList(input);
            return DoTheMagic(ref register, ipIndex, operations);
        }

        protected override string FindSecondSolution(string[] input)
        {
            int[] register = new int[] { 1, 0, 0, 0, 0, 0 };
            int ipIndex = int.Parse(input[0].Replace("#ip ", ""));
            Dictionary<int, string> operations = GetOperationsList(input);
            return DoTheMagic2(ref register, ipIndex, operations);
        }

        private string DoTheMagic(ref int[] register, int ipIndex, Dictionary<int, string> operations)
        {
            while (true)
            {
                string op = operations[register[ipIndex]];

                if (op.Split(" ")[0] == "addr")
                    register = Day16.Program.addr(op, register);
                else if (op.Split(" ")[0] == "addi")
                    register = Day16.Program.addi(op, register);
                else if (op.Split(" ")[0] == "mulr")
                    register = Day16.Program.mulr(op, register);
                else if (op.Split(" ")[0] == "muli")
                    register = Day16.Program.muli(op, register);
                else if (op.Split(" ")[0] == "banr")
                    register = Day16.Program.banr(op, register);
                else if (op.Split(" ")[0] == "bani")
                    register = Day16.Program.bani(op, register);
                else if (op.Split(" ")[0] == "borr")
                    register = Day16.Program.borr(op, register);
                else if (op.Split(" ")[0] == "bori")
                    register = Day16.Program.bori(op, register);
                else if (op.Split(" ")[0] == "setr")
                    register = Day16.Program.setr(op, register);
                else if (op.Split(" ")[0] == "seti")
                    register = Day16.Program.seti(op, register);
                else if (op.Split(" ")[0] == "gtir")
                    register = Day16.Program.gtir(op, register);
                else if (op.Split(" ")[0] == "gtri")
                    register = Day16.Program.gtri(op, register);
                else if (op.Split(" ")[0] == "gtrr")
                    register = Day16.Program.gtrr(op, register);
                else if (op.Split(" ")[0] == "eqir")
                    register = Day16.Program.eqir(op, register);
                else if (op.Split(" ")[0] == "eqri")
                    register = Day16.Program.eqri(op, register);
                else if (op.Split(" ")[0] == "eqrr")
                    register = Day16.Program.eqrr(op, register);
                else
                    throw new Exception("huh!!?");

                if (register[ipIndex] >= operations.Count - 1)
                {
                    break;
                }

                register[ipIndex] += 1;
            }

            return register[0].ToString();
        }

        private string DoTheMagic2(ref int[] register, int ipIndex, Dictionary<int, string> operations)
        {
            int magicNumber = 10551383;

            while (true)
            {
                string op = operations[register[ipIndex]];

                if (op.Split(" ")[0] == "addr")
                    register = Day16.Program.addr(op, register);
                else if (op.Split(" ")[0] == "addi")
                    register = Day16.Program.addi(op, register);
                else if (op.Split(" ")[0] == "mulr")
                    register = Day16.Program.mulr(op, register);
                else if (op.Split(" ")[0] == "muli")
                    register = Day16.Program.muli(op, register);
                else if (op.Split(" ")[0] == "banr")
                    register = Day16.Program.banr(op, register);
                else if (op.Split(" ")[0] == "bani")
                    register = Day16.Program.bani(op, register);
                else if (op.Split(" ")[0] == "borr")
                    register = Day16.Program.borr(op, register);
                else if (op.Split(" ")[0] == "bori")
                    register = Day16.Program.bori(op, register);
                else if (op.Split(" ")[0] == "setr")
                    register = Day16.Program.setr(op, register);
                else if (op.Split(" ")[0] == "seti")
                    register = Day16.Program.seti(op, register);
                else if (op.Split(" ")[0] == "gtir")
                    register = Day16.Program.gtir(op, register);
                else if (op.Split(" ")[0] == "gtri")
                    register = Day16.Program.gtri(op, register);
                else if (op.Split(" ")[0] == "gtrr")
                    register = Day16.Program.gtrr(op, register);
                else if (op.Split(" ")[0] == "eqir")
                    register = Day16.Program.eqir(op, register);
                else if (op.Split(" ")[0] == "eqri")
                    register = Day16.Program.eqri(op, register);
                else if (op.Split(" ")[0] == "eqrr")
                    register = Day16.Program.eqrr(op, register);
                else
                    throw new Exception("huh!!?");

                register[ipIndex] += 1;

                //here I should find a cycle - maybe later :)
                if (register[1] == magicNumber && register[4] == 1 && register[ipIndex] == 3)
                {
                    break;
                }
            }

            List<int> dividors = new List<int>();
            for (int i = 1; i <= magicNumber; i++)
            {
                if (magicNumber % i == 0)
                {
                    dividors.Add(i);
                }
            }

            //now we simulate the above listed operations - we just skip incrementing by one and instead increment by dividors of magic number
            //- dividors of magic number can cause to increment register 0.
            while (true)
            {
                register[3] = register[2] * register[4];
                register[3] = (register[3] == register[1] ? 1 : 0);

                if (register[3] == 1)
                {
                    register[0] += register[2];
                }

                register[4]++;
                while (!dividors.Contains(register[4]))
                {
                    if (register[4] > magicNumber)
                    {
                        break;
                    }
                    register[4]++;
                }

                register[3] = register[4] > register[1] ? 1 : 0;
                if (register[3] == 1)
                {
                    register[2]++;
                    while (!dividors.Contains(register[2]))
                    {
                        if (register[2] > magicNumber)
                        {
                            break;
                        }
                        register[2]++;
                    }

                    register[3] = register[2] > register[1] ? 1 : 0;

                    if (register[3] == 1)
                    {
                        return register[0].ToString();
                    }
                    else
                    {
                        register[4] = 1;
                    }
                }
            }

            throw new Exception("should not happend");
        }

        private static Dictionary<int, string> GetOperationsList(string[] input)
        {
            Dictionary<int, string> operations = new Dictionary<int, string>();
            for (int i = 1; i < input.Length; i++)
            {
                operations.Add(i - 1, input[i]);
            }

            return operations;
        }
    }
}