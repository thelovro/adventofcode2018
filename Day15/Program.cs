﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Day15
{
    class Program : BaseAoc
    {
        static void Main(string[] args)
        {
            Program program = new Program();
            program.SampleInputPartOne.Add(@"#######
#.G...#
#...EG#
#.#.#G#
#..G#E#
#.....#
#######", "27730");

            program.SampleInputPartOne.Add(@"#######
#G..#E#
#E#E.E#
#G.##.#
#...#E#
#...E.#
#######", "36334");

            program.SampleInputPartOne.Add(@"#######
#E..EG#
#.#G.E#
#E.##E#
#G..#.#
#..E#.#
#######", "39514");

            program.SampleInputPartOne.Add(@"#######
#E.G#.#
#.#G..#
#G.#.G#
#G..#.#
#...E.#
#######", "27755");

            program.SampleInputPartOne.Add(@"#######
#.E...#
#.#..G#
#.###.#
#E#G#G#
#...#G#
#######", "28944");

            program.SampleInputPartOne.Add(@"#########
#G......#
#.E.#...#
#..##..G#
#...##..#
#...#...#
#.G...G.#
#.....G.#
#########", "18740");

            program.SampleInputPartOne.Add(@"################
#.......G......#
#G.............#
#..............#
#....###########
#....###########
#.......EG.....#
################", "18468"); // (486 x 38)

            program.Solve();
        }

        protected override string FindFirstSolution(string[] input)
        {
            (List<Warrior> warriors, string[,] map) = FindAllWarriorsAndMap(input);

            //initial map
            if (IsTest)
            {
                DrawMap(map, warriors);
            }

            (List<Warrior> warriorsAtTheEnd, int rounds) = PlayGame(warriors, map);

            int score = warriorsAtTheEnd.Where(x => !x.IsDead).Sum(x => x.Health) * (rounds);
            return score.ToString();
        }

        protected override string FindSecondSolution(string[] input)
        {
            List<Warrior> warriorsAtTheEnd;
            int rounds;
            int elfHitPower = 20;
            while (true)
            {
                (List<Warrior> warriors, string[,] map) = FindAllWarriorsAndMap(input);
                int numberOfElfs = warriors.Where(x => x.Type == Warrior.WarriorType.Elf).Count();

                //initial map
                if (IsTest)
                {
                    DrawMap(map, warriors);
                }

                (List<Warrior> warriorsTmp, int roundsTmp) = PlayGame(warriors, map, elfHitPower);

                int numberOfElfsInGame = warriorsTmp.Where(x => !x.IsDead && x.Type == Warrior.WarriorType.Elf).Count();
                if (numberOfElfs == numberOfElfsInGame)
                {
                    warriorsAtTheEnd = warriorsTmp;
                    rounds = roundsTmp;
                    break;
                }

                elfHitPower++;
            }

            int score = warriorsAtTheEnd.Where(x => !x.IsDead).Sum(x => x.Health) * (rounds);
            return score.ToString();
        }

        private (List<Warrior> warriors, int rounds) PlayGame(List<Warrior> warriors, string[,] map, int elfHitStrength = 3)
        {
            int goblinHitStrength = 3;
            int rounds = 0;
            bool hasGameEnded = false;

            while (true)
            {
                foreach (var warrior in warriors.OrderBy(x => x.PositionY).ThenBy(x => x.PositionX))
                {
                    if (warrior.IsDead)
                    {
                        continue;
                    }

                    var enemyType = warrior.Type == Warrior.WarriorType.Goblin ? Warrior.WarriorType.Elf : Warrior.WarriorType.Goblin;

                    Location enemyWithLeastPowerNearMe = GetEnemyWithLeastPowerInAdjacentSquares(warrior.PositionX, warrior.PositionY, enemyType, warriors);

                    //no need to search if we have an enemy near me!!!
                    if (enemyWithLeastPowerNearMe == null)
                    {
                        if (!warriors.Any(x => x.Type == enemyType && !x.IsDead))
                        {
                            hasGameEnded = true;
                            break;
                        }

                        Location nextStep = FindShortestPath(warrior, map, warriors);

                        if (nextStep != null)
                        {
                            warrior.PositionX = nextStep.PositionX;
                            warrior.PositionY = nextStep.PositionY;
                        }

                        enemyWithLeastPowerNearMe = GetEnemyWithLeastPowerInAdjacentSquares(warrior.PositionX, warrior.PositionY, enemyType, warriors);
                    }

                    //ATTAAACKKK!!!!
                    if (enemyWithLeastPowerNearMe != null)
                    {
                        warriors.First(x => !x.IsDead && x.PositionX == enemyWithLeastPowerNearMe.PositionX && x.PositionY == enemyWithLeastPowerNearMe.PositionY).ReceiveHit(warrior.Type == Warrior.WarriorType.Elf ? elfHitStrength : goblinHitStrength);
                    }
                }

                rounds++;

                if (hasGameEnded)
                {
                    break;
                }
            }

            if (IsTest)
            {
                DrawMap(map, warriors);
                Console.WriteLine("rounds: " + (rounds - 1));
                foreach (var bla in warriors.OrderBy(x => x.PositionY).ThenBy(x => x.PositionX))
                {
                    Console.WriteLine(string.Format("x: {0}, y:{1}, health: {2}, type: {3}", bla.PositionX, bla.PositionY, bla.Health, bla.Type));
                }
            }

            return (warriors, rounds - 1);
        }

        private Location FindShortestPath(Warrior warrior, string[,] map, List<Warrior> warriors)
        {
            Location current = null;
            var start = new Location { PositionX = warrior.PositionX, PositionY = warrior.PositionY };
            var closedList = new List<Location>();

            Queue<Location> openList = new Queue<Location>();

            // start by adding the original position to the open list
            openList.Enqueue(start);

            List<Location> nearestEnemies = new List<Location>();

            while (openList.Count > 0)
            {
                current = openList.Dequeue();

                // add the current square to the closed list
                closedList.Add(current);

                Location adjacentToEnemy = GetEnemyWithLeastPowerInAdjacentSquares(current.PositionX, current.PositionY, warrior.Type == Warrior.WarriorType.Elf ? Warrior.WarriorType.Goblin : Warrior.WarriorType.Elf, warriors);

                if (adjacentToEnemy != null)
                {
                    if (nearestEnemies.Count == 0)
                    {
                        nearestEnemies.Add(current);
                    }
                    else if (nearestEnemies[0].CurrentPathLength == current.CurrentPathLength)
                    {
                        nearestEnemies.Add(current);
                    }
                    else
                    {
                        //we have bigger path length - in this case we have to break!
                        break;
                    }
                }

                var adjacentSquares = GetWalkableAdjacentSquares(current, map, warriors, openList, closedList);
                foreach (var item in adjacentSquares)
                {
                    openList.Enqueue(item);
                }
            }

            if (nearestEnemies.Count == 0)
            {
                return null;
            }

            var enemyLocation = nearestEnemies.OrderBy(e => e.PositionY).ThenBy(e => e.PositionX).First();
            var nextStep = enemyLocation;
            while (nextStep.Parent.Parent != null)
            {
                nextStep = nextStep.Parent;
            }

            return nextStep;
        }

        static List<Location> GetWalkableAdjacentSquares(Location parent, string[,] map, List<Warrior> warriors, Queue<Location> openList, List<Location> closedList)
        {
            var proposedLocations = new List<Location>()
            {
                new Location { PositionX = parent.PositionX, PositionY = parent.PositionY - 1, Parent = parent, CurrentPathLength = parent.CurrentPathLength + 1 },
                new Location { PositionX = parent.PositionX - 1, PositionY = parent.PositionY, Parent = parent, CurrentPathLength = parent.CurrentPathLength + 1 },
                new Location { PositionX = parent.PositionX + 1, PositionY = parent.PositionY, Parent = parent, CurrentPathLength = parent.CurrentPathLength + 1 },
                new Location { PositionX = parent.PositionX, PositionY = parent.PositionY + 1, Parent = parent, CurrentPathLength = parent.CurrentPathLength + 1 },
            };

            return proposedLocations
                .Where(x => !closedList.Any(c => c.PositionX == x.PositionX && c.PositionY == x.PositionY) && !openList.Any(c => c.PositionX == x.PositionX && c.PositionY == x.PositionY))
                .Where(l => IsPositionFree(l.PositionX, l.PositionY, map, warriors)).ToList();
        }

        static Location GetEnemyWithLeastPowerInAdjacentSquares(int x, int y, Warrior.WarriorType warriorType, List<Warrior> warriors)
        {
            //order of enemies is important!!
            var enemyUp = warriors.FirstOrDefault(w => w.PositionX == x && w.PositionY == y - 1 && w.Type == warriorType && !w.IsDead);
            var enemyLeft = warriors.FirstOrDefault(w => w.PositionX == x - 1 && w.PositionY == y && w.Type == warriorType && !w.IsDead);
            var enemyRight = warriors.FirstOrDefault(w => w.PositionX == x + 1 && w.PositionY == y && w.Type == warriorType && !w.IsDead);
            var enemyDown = warriors.FirstOrDefault(w => w.PositionX == x && w.PositionY == y + 1 && w.Type == warriorType && !w.IsDead);

            int minHealth = int.MaxValue;
            Location locationToAttack = null;
            if (enemyUp != null)
            {
                if (enemyUp.Health < minHealth)
                {
                    minHealth = enemyUp.Health;
                    locationToAttack = new Location() { PositionX = enemyUp.PositionX, PositionY = enemyUp.PositionY };
                }
            }

            if (enemyLeft != null)
            {
                if (enemyLeft.Health < minHealth)
                {
                    minHealth = enemyLeft.Health;
                    locationToAttack = new Location() { PositionX = enemyLeft.PositionX, PositionY = enemyLeft.PositionY };
                }
            }

            if (enemyRight != null)
            {
                if (enemyRight.Health < minHealth)
                {
                    minHealth = enemyRight.Health;
                    locationToAttack = new Location() { PositionX = enemyRight.PositionX, PositionY = enemyRight.PositionY };
                }
            }

            if (enemyDown != null)
            {
                if (enemyDown.Health < minHealth)
                {
                    minHealth = enemyDown.Health;
                    locationToAttack = new Location() { PositionX = enemyDown.PositionX, PositionY = enemyDown.PositionY };
                }
            }

            return locationToAttack;
        }

        private static bool IsPositionFree(int x, int y, string[,] map, List<Warrior> warriors)
        {
            if (map[y, x] == "#")
            {
                return false;
            }

            return !warriors.Where(w => !w.IsDead).Any(w => w.PositionX == x && w.PositionY == y);
        }

        private (List<Warrior>, string[,]) FindAllWarriorsAndMap(string[] input)
        {
            string[,] map = new string[input.Length, input[0].Length];
            List<Warrior> warriors = new List<Warrior>();
            int warriorCounter = 0;
            for (int y = 0; y < input.Length; y++)
            {
                for (int x = 0; x < input[y].Length; x++)
                {
                    char character = input[y][x];
                    if (character == '#' || character == '.')
                    {
                        map[y, x] = character.ToString();
                    }
                    else
                    {
                        map[y, x] = ".";

                        warriors.Add(new Warrior()
                        {
                            ID = warriorCounter++,
                            Type = (character == 'G' ? Warrior.WarriorType.Goblin : Warrior.WarriorType.Elf),
                            PositionX = x,
                            PositionY = y
                        });
                    }
                }
            }

            return (warriors, map);
        }

        private void DrawMap(string[,] map, List<Warrior> warriors)
        {
            for (int y = 0; y <= map.GetUpperBound(0); y++)
            {
                for (int x = 0; x <= map.GetUpperBound(1); x++)
                {
                    if (warriors.Where(a => !a.IsDead).Any(a => a.PositionX == x && a.PositionY == y))
                    {
                        Console.Write(warriors.Where(a => !a.IsDead && a.PositionX == x && a.PositionY == y).First().Type == Warrior.WarriorType.Goblin ? "G" : "E");
                    }
                    else
                    {
                        Console.Write(map[y, x]);
                    }
                }
                Console.WriteLine();
            }
        }
    }

    public class Location
    {
        public int PositionX;
        public int PositionY;
        public int CurrentPathLength { get; set; }
        public int ManhattanToFinal { get; set; }
        public Location Parent;
    }

    public class Warrior
    {
        public enum WarriorType
        {
            Elf,
            Goblin
        }

        public int ID { get; set; }
        public WarriorType Type { get; set; }
        private int _health = 200;
        public int Health { get { return _health; } }
        public void ReceiveHit(int elfHitStrength)
        {
            _health -= elfHitStrength;
        }
        public bool IsDead { get { return _health <= 0; } }
        public int PositionX { get; set; }
        public int PositionY { get; set; }
        public (int, int) Positon { get { return (PositionX, PositionY); } }
    }
}