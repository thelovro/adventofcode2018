﻿using System;
using System.Linq;
using System.Collections.Generic;
using Common;

namespace Day03
{
    class Program : BaseAoc
    {
        static void Main(string[] args)
        {
            Program program = new Program();
            program.SampleInputPartOne.Add(@"#1 @ 1,3: 4x4
#2 @ 3,1: 4x4
#3 @ 5,5: 2x2", "4");

            program.Solve();
        }

        protected override string FindFirstSolution(string[] input)
        {
            List<Line> lines = ParseInput(input);
            int[,] fabricField = PrepareFabricField(lines);

            //calculate overlaps
            int numberOfOverlaps = 0;
            for (int i = 0; i < 1100; i++)
            {
                for (int j = 0; j < 1100; j++)
                {
                    if (fabricField[i, j] > 1)
                    {
                        numberOfOverlaps++;
                    }
                }
            }

            return numberOfOverlaps.ToString();
        }

        protected override string FindSecondSolution(string[] input)
        {
            List<Line> lines = ParseInput(input);
            int[,] fabricField = PrepareFabricField(lines);

            foreach (var line in lines)
            {
                bool isOverlaped = false;
                for (int i = line.Left; i < line.Left + line.Width; i++)
                {
                    for (int j = line.Top; j < line.Top + line.Height; j++)
                    {
                        if (fabricField[i, j] > 1)
                        {
                            isOverlaped = true;
                        }
                    }
                }

                if (!isOverlaped)
                {
                    return line.ID.TrimStart('#');
                }
            }

            return "this should not happend!";
        }

        private List<Line> ParseInput(string[] input)
        {
            List<Line> lines = new List<Line>();
            foreach (string line in input)
            {
                string[] lineItems = line.Split(" ");
                lines.Add(new Line()
                {
                    ID = lineItems[0],
                    Left = int.Parse(lineItems[2].TrimEnd(':').Split(',')[0]),
                    Top = int.Parse(lineItems[2].TrimEnd(':').Split(',')[1]),
                    Width = int.Parse(lineItems[3].Split("x")[0]),
                    Height = int.Parse(lineItems[3].Split("x")[1])
                });
            }

            return lines;
        }

        private static int[,] PrepareFabricField(List<Line> lines)
        {
            int[,] fabricField = new int[1100, 1100];
            foreach (var line in lines)
            {
                for (int i = line.Left; i < line.Left + line.Width; i++)
                {
                    for (int j = line.Top; j < line.Top + line.Height; j++)
                    {
                        fabricField[i, j] += 1;
                    }
                }
            }

            return fabricField;
        }
    }

    public class Line
    {
        public string ID;
        public int Left;
        public int Top;

        public int Width;
        public int Height;
    }
}