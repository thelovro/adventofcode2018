﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Day01
{
    class Program : BaseAoc
    {
        static void Main(string[] args)
        {
            Program program = new Program();
            program.SampleInputPartOne.Add(@"+1
-2
+3
+1", "3");

            program.Solve();
        }

        protected override string FindFirstSolution(string[] input)
        {
            int result = 0;
            foreach (var row in input)
            {
                result += int.Parse(row);
            }

            return result.ToString();
        }

        protected override string FindSecondSolution(string[] input)
        {
            int counter = 0;
            int result = 0;
            List<int> frequences = new List<int>();
            while (counter < input.Length)
            {
                result += int.Parse(input[counter]);
                if (!frequences.Contains(result))
                {
                    frequences.Add(result);
                }
                else
                {
                    break;
                }

                if (++counter == input.Length)
                {
                    counter = 0;
                }
            }

            return result.ToString();
        }
    }
}