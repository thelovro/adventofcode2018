﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Day24
{
    class Program : BaseAoc
    {
        static void Main(string[] args)
        {
            Program program = new Program();
            program.SampleInputPartOne.Add(@"Immune System:
17 units each with 5390 hit points (weak to radiation, bludgeoning) with an attack that does 4507 fire damage at initiative 2
989 units each with 1274 hit points (immune to fire; weak to bludgeoning, slashing) with an attack that does 25 slashing damage at initiative 3

Infection:
801 units each with 4706 hit points (weak to radiation) with an attack that does 116 bludgeoning damage at initiative 1
4485 units each with 2961 hit points (immune to radiation; weak to fire, cold) with an attack that does 12 slashing damage at initiative 4"
,"5216");

            program.SampleInputPartTwo.Add(@"Immune System:
17 units each with 5390 hit points (weak to radiation, bludgeoning) with an attack that does 4507 fire damage at initiative 2
989 units each with 1274 hit points (immune to fire; weak to bludgeoning, slashing) with an attack that does 25 slashing damage at initiative 3

Infection:
801 units each with 4706 hit points (weak to radiation) with an attack that does 116 bludgeoning damage at initiative 1
4485 units each with 2961 hit points (immune to radiation; weak to fire, cold) with an attack that does 12 slashing damage at initiative 4"
,"51");

            program.Solve();
        }

        protected override string FindFirstSolution(string[] input)
        {
            List<Group> groupList = ParseInput(input);
            DetermineTargetsAndFight(groupList);
            return groupList.Sum(d => d.NumberOfUnits).ToString();
        }

        protected override string FindSecondSolution(string[] input)
        {
            int boost = 5;
            while (true)
            {
                List<Group> groupList = ParseInput(input, boost++);
                DetermineTargetsAndFight(groupList);

                if (groupList.Any(g => g.Type == Group.TypeEnum.ImmuneSystem && g.NumberOfUnits > 0)
                    && !groupList.Any(g => g.Type == Group.TypeEnum.Infection && g.NumberOfUnits > 0))
                {
                    return groupList.Sum(d => d.NumberOfUnits).ToString();
                }
            }
        }

        private void DetermineTargetsAndFight(List<Group> groupList)
        {
            int counter = 0;
            int numberOfUnits = 0;
            //we fight until only one type of groups remain
            while (groupList.Where(g => g.NumberOfUnits > 0).Select(g => g.Type).Distinct().Count() > 1)
            {
                numberOfUnits = groupList.Sum(g => g.NumberOfUnits);
                Dictionary<int, int> groupsUnderAttack = new Dictionary<int, int>();

                //determine target
                foreach (Group attacker in groupList.Where(g => g.NumberOfUnits > 0).OrderByDescending(g => g.AttackPowerPerGroup).ThenByDescending(g => g.Initiative))
                {
                    var attackerType = attacker.Type;
                    var attackType = attacker.AttackType;

                    var defendingGroups = groupList
                                            .Where(g => g.NumberOfUnits > 0)
                                            .Where(g => !groupsUnderAttack.ContainsValue(g.ID))
                                            .Where(g => !g.Immunities.Contains(attacker.AttackType))
                                            .Where(g => g.Type != attackerType);

                    Dictionary<int, (int, int, int)> defenders = new Dictionary<int, (int, int, int)>();
                    foreach (var defender in defendingGroups)
                    {
                        int factor = 1;
                        if (defender.Weaknesses.Contains(attacker.AttackType))
                        {
                            factor = 2;
                        }
                        else if (defender.Immunities.Contains(attacker.AttackType))
                        {
                            factor = 0;
                        }

                        defenders.Add(defender.ID, (attacker.AttackPowerPerGroup * factor, defender.AttackPowerPerGroup, defender.Initiative));
                    }

                    int? selectedDefenderId = null;

                    if (defenders.Any())
                    {
                        selectedDefenderId = defenders
                            .OrderByDescending(d => d.Value.Item1)
                            .ThenByDescending(d => d.Value.Item2)
                            .ThenByDescending(d => d.Value.Item3)
                            .Select(d => d.Key)
                            .First();
                    }

                    if (selectedDefenderId.HasValue)
                    {
                        groupsUnderAttack.Add(attacker.ID, selectedDefenderId.Value);
                    }
                }

                //attack!!
                foreach (Group attacker in groupList.Where(g => g.NumberOfUnits > 0).OrderByDescending(g => g.Initiative))
                {
                    if (attacker.NumberOfUnits == 0)
                    {
                        continue;
                    }

                    if (!groupsUnderAttack.ContainsKey(attacker.ID))
                    {
                        continue;
                    }
                    var defender = groupList.First(g => g.ID == groupsUnderAttack[attacker.ID]);

                    defender.ReceiveHit(attacker.AttackType, attacker.AttackPowerPerGroup);
                }

                //we need some kind of safety - in between the units don't have enough power to kill one enemy and we get a cycle
                if (numberOfUnits == groupList.Sum(g => g.NumberOfUnits))
                {
                    counter++;
                }

                if (counter == 10)
                {
                    break;
                }
            }
        }

        private List<Group> ParseInput(string[] input, int boost = 0)
        {
            List<Group> groupList = new List<Group>();
            Group.TypeEnum type = Group.TypeEnum.ImmuneSystem;
            int counter = 0;
            foreach (var row in input)
            {
                if (row == "")
                {
                    continue;
                }
                else if (row == "Immune System:")
                {
                    type = Group.TypeEnum.ImmuneSystem;
                    continue;
                }
                else if (row == "Infection:")
                {
                    type = Group.TypeEnum.Infection;
                    continue;
                }
                Group group;
                Regex regex = new Regex(@"(?<units>\d+) units each with (?<hitPoints>\d+) hit points.*with an attack that does (?<damage>\d+) (?<attackType>.+) damage at initiative (?<initiative>\d+)");
                if (regex.IsMatch(row))
                {
                    var match = regex.Match(row);
                    group = new Group()
                    {
                        NumberOfUnits = int.Parse(match.Groups["units"].Value),
                        HitPointsPerUnit = int.Parse(match.Groups["hitPoints"].Value),
                        AttackPowerPerUnit = int.Parse(match.Groups["damage"].Value) + (type == Group.TypeEnum.ImmuneSystem ? boost : 0),
                        Initiative = int.Parse(match.Groups["initiative"].Value),
                        Type = type,
                        ID = counter
                    };
                    switch (match.Groups["attackType"].Value)
                    {
                        case "cold":
                            group.AttackType = Group.DamageTypeEnum.Cold;
                            break;
                        case "radiation":
                            group.AttackType = Group.DamageTypeEnum.Radiation;
                            break;
                        case "slashing":
                            group.AttackType = Group.DamageTypeEnum.Slashing;
                            break;
                        case "bludgeoning":
                            group.AttackType = Group.DamageTypeEnum.Bludgeoning;
                            break;
                        case "fire":
                            group.AttackType = Group.DamageTypeEnum.Fire;
                            break;
                    }
                }
                else
                {
                    throw new Exception("should not happend");
                }

                if (row.Contains('('))
                {
                    //we have immunities and/or weaknesses
                    var data = row.Substring(row.IndexOf('(') + 1, row.IndexOf(')') - row.IndexOf('(') - 1);
                    foreach (var item in data.Split("; "))
                    {
                        if (item.StartsWith("weak to "))
                        {
                            foreach (var weakness in item.Replace("weak to ", "").Split(", "))
                            {
                                switch (weakness)
                                {
                                    case "cold":
                                        group.Weaknesses.Add(Group.DamageTypeEnum.Cold);
                                        break;
                                    case "radiation":
                                        group.Weaknesses.Add(Group.DamageTypeEnum.Radiation);
                                        break;
                                    case "slashing":
                                        group.Weaknesses.Add(Group.DamageTypeEnum.Slashing);
                                        break;
                                    case "bludgeoning":
                                        group.Weaknesses.Add(Group.DamageTypeEnum.Bludgeoning);
                                        break;
                                    case "fire":
                                        group.Weaknesses.Add(Group.DamageTypeEnum.Fire);
                                        break;
                                }
                            }
                        }
                        else if (item.StartsWith("immune to "))
                        {
                            foreach (var immunity in item.Replace("immune to ", "").Split(", "))
                            {
                                switch (immunity)
                                {
                                    case "cold":
                                        group.Immunities.Add(Group.DamageTypeEnum.Cold);
                                        break;
                                    case "radiation":
                                        group.Immunities.Add(Group.DamageTypeEnum.Radiation);
                                        break;
                                    case "slashing":
                                        group.Immunities.Add(Group.DamageTypeEnum.Slashing);
                                        break;
                                    case "bludgeoning":
                                        group.Immunities.Add(Group.DamageTypeEnum.Bludgeoning);
                                        break;
                                    case "fire":
                                        group.Immunities.Add(Group.DamageTypeEnum.Fire);
                                        break;
                                }
                            }
                        }
                    }
                }

                groupList.Add(group);
                counter++;
            }

            return groupList;
        }
    }

    public class Group
    {
        public enum DamageTypeEnum
        {
            Cold,
            Radiation,
            Slashing,
            Bludgeoning,
            Fire
        }

        public enum TypeEnum
        {
            ImmuneSystem,
            Infection
        }

        public int ID;
        public int NumberOfUnits;
        public int HitPointsPerUnit;
        public int HitPointsPerGroup { get { return NumberOfUnits * HitPointsPerUnit; } }
        public int AttackPowerPerUnit;
        public int AttackPowerPerGroup { get { return NumberOfUnits * AttackPowerPerUnit; } }
        public TypeEnum Type;
        public DamageTypeEnum AttackType;
        public int Initiative;
        public List<DamageTypeEnum> Immunities = new List<DamageTypeEnum>();
        public List<DamageTypeEnum> Weaknesses = new List<DamageTypeEnum>();
        public void ReceiveHit(DamageTypeEnum damageType, int attackPower)
        {
            if (Immunities.Contains(damageType))
            {
                return;
            }

            if (Weaknesses.Contains(damageType))
            {
                attackPower *= 2;
            }

            var remainingHitPoints = HitPointsPerGroup - attackPower;
            if (remainingHitPoints <= 0)
            {
                NumberOfUnits = 0;
            }
            else
            {
                NumberOfUnits = (int)Math.Ceiling((double)remainingHitPoints / (double)HitPointsPerUnit);
            }
        }
    }
}