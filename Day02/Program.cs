﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Day02
{
    class Program : BaseAoc
    {
        static void Main(string[] args)
        {
            Program program = new Program();
            program.SampleInputPartOne.Add(@"abcdef
bababc
abbcde
abcccd
aabcdd
abcdee
ababab", "12");

            program.SampleInputPartTwo.Add(@"abcde
fghij
klmno
pqrst
fguij
axcye
wvxyz", "fgij");

            program.Solve();
        }

        protected override string FindFirstSolution(string[] input)
        {
            int numbefOfTwos = 0;
            int numbefOfThrees = 0;
            foreach (string id in input)
            {
                var cArray = id.ToCharArray();
                Array.Sort(cArray);

                IEnumerable<int> countedChars = new List<int>();
                foreach (char c in cArray)
                {
                    countedChars = cArray.GroupBy(x => x).Select(y => y.Count());
                }

                if (countedChars.Contains(3))
                {
                    numbefOfThrees++;
                }
                if (countedChars.Contains(2))
                {
                    numbefOfTwos++;
                }
            }

            return (numbefOfThrees * numbefOfTwos).ToString();
        }

        protected override string FindSecondSolution(string[] input)
        {
            for (int i = 0; i < input.Length; i++)
            {
                for (int j = 0; j < input[i].Length; j++)
                {
                    for (int k = 0; k < input.Length; k++)
                    {
                        if (i == k)
                        {
                            continue;
                        }

                        if (input[i].Remove(input[i].IndexOf(input[i][j]), 1) == input[k].Remove(input[k].IndexOf(input[k][j]), 1))
                        {
                            return input[i].Remove(input[i].IndexOf(input[i][j]), 1);
                        }
                    }
                }
            }

            return "this should not happend";
        }
    }
}