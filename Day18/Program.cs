﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Day18
{
    class Program : BaseAoc
    {
        static void Main(string[] args)
        {
            Program program = new Program();
            program.SampleInputPartOne.Add(@".#.#...|#.
.....#|##|
.|..|...#.
..|#.....#
#.#|||#|#|
...#.||...
.|....|...
||...#|.#|
|.||||..|.
...#.|..|.", "1147");

            program.Solve();
        }

        protected override string FindFirstSolution(string[] input)
        {
            string[,] map = ParseMap(input);
            if (IsTest)
            {
                DrawMap(map);
            }

            string[,] tmpMap = (string[,])map.Clone();

            //10 minutes = 10 cycles
            for (int i = 0; i < 10; i++)
            {
                for (int y = 0; y <= map.GetUpperBound(0); y++)
                {
                    for (int x = 0; x <= map.GetUpperBound(1); x++)
                    {
                        tmpMap[y, x] = CalculateNextMinuteItem(map, x, y, map.GetUpperBound(0));
                    }
                }

                map = (string[,])tmpMap.Clone();
            }

            if (IsTest)
            {
                DrawMap(map);
            }

            return (CountWoodedAcres(map) * CountLumberyards(map)).ToString();
        }

        protected override string FindSecondSolution(string[] input)
        {
            string[,] map = ParseMap(input);
            string[,] tmpMap = (string[,])map.Clone();

            List<string[,]> results = new List<string[,]>();

            int firstIterations = 0;
            int cycleSize = 0;
            //1000000000 minutes = 1000000000 cycles
            for (int i = 0; i < 1000000000; i++)
            {
                for (int y = 0; y <= map.GetUpperBound(0); y++)
                {
                    for (int x = 0; x <= map.GetUpperBound(1); x++)
                    {
                        tmpMap[y, x] = CalculateNextMinuteItem(map, x, y, map.GetUpperBound(0));
                    }
                }

                map = (string[,])tmpMap.Clone();

                foreach (var m in results)
                {
                    if (AreArraysTheSame(m, map))
                    {
                        firstIterations = results.IndexOf(m);
                        cycleSize = i - firstIterations;
                        break;
                    }
                }

                //did we find the cycle?
                if (cycleSize > 0)
                {
                    break;
                }

                results.Add(map);
            }

            int numberOfLoops = ((1000000000 - firstIterations) % cycleSize) + firstIterations;

            //repeat everthing again - but only for calculated number of loops!
            map = ParseMap(input);
            tmpMap = (string[,])map.Clone();

            for (int i = 0; i < numberOfLoops; i++)
            {
                for (int y = 0; y <= map.GetUpperBound(0); y++)
                {
                    for (int x = 0; x <= map.GetUpperBound(1); x++)
                    {
                        tmpMap[y, x] = CalculateNextMinuteItem(map, x, y, map.GetUpperBound(0));
                    }
                }

                map = (string[,])tmpMap.Clone();
            }

            return (CountWoodedAcres(map) * CountLumberyards(map)).ToString();
        }

        private string CalculateNextMinuteItem(string[,] map, int x, int y, int maxSize)
        {
            List<string> fields = new List<string>();
            for (int j = (y > 0 ? y - 1 : 0); j <= (y < maxSize ? y + 1 : y); j++)
            {
                for (int i = (x > 0 ? x - 1 : 0); i <= (x < maxSize ? x + 1 : x); i++)
                {
                    if (i == x && j == y)
                    {
                        continue;
                    }
                    fields.Add(map[j, i]);
                }
            }

            if (map[y, x] == "." && fields.Where(f => f == "|").Count() > 2)
            {
                return "|";
            }
            else if (map[y, x] == "|" && fields.Where(f => f == "#").Count() > 2)
            {
                return "#";
            }
            else if (map[y, x] == "#" && fields.Where(f => f == "|").Count() > 0 && fields.Where(f => f == "#").Count() > 0)
            {
                return "#";
            }
            else if (map[y, x] == "#" && (fields.Where(f => f == "|").Count() == 0 || fields.Where(f => f == "#").Count() == 0))
            {
                return ".";
            }
            else
            {
                return map[y, x];
            }
        }

        private string[,] ParseMap(string[] input)
        {
            string[,] map = new string[input.Length, input[0].Length];

            for (int y = 0; y < input.Length; y++)
            {
                for (int x = 0; x < input[0].Length; x++)
                {
                    map[y, x] = input[y][x].ToString();
                }
            }

            return map;
        }

        private int CountWoodedAcres(string[,] map)
        {
            return CountCharacters(map, "|");
        }

        private int CountLumberyards(string[,] map)
        {
            return CountCharacters(map, "#");
        }

        private int CountCharacters(string[,] map, string character)
        {
            int counter = 0;
            for (int y = 0; y <= map.GetUpperBound(0); y++)
            {
                for (int x = 0; x <= map.GetUpperBound(1); x++)
                {
                    if (map[y, x] == character)
                    {
                        counter++;
                    }
                }
            }

            return counter;
        }

        private void DrawMap(string[,] map)
        {
            for (int y = 0; y <= map.GetUpperBound(0); y++)
            {
                for (int x = 0; x <= map.GetUpperBound(1); x++)
                {
                    Console.Write(map[y, x]);
                }
                Console.WriteLine();
            }

            Console.WriteLine();
        }
        
        private bool AreArraysTheSame(string[,] map1, string[,] map2)
        {
            return map1.Rank == map2.Rank &&
                        Enumerable.Range(0, map1.Rank).All(dimension => map1.GetLength(dimension) == map2.GetLength(dimension)) &&
                        map1.Cast<string>().SequenceEqual(map2.Cast<string>());
        }
    }
}