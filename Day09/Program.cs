﻿using System;
using System.Linq;
using System.Collections.Generic;
using Common;

namespace Day09
{
    class Program : BaseAoc
    {
        static void Main(string[] args)
        {
            Program program = new Program();
            program.SampleInputPartOne.Add(@"9;25", "32");
            program.SampleInputPartOne.Add(@"10;1618", "8317");
            program.SampleInputPartOne.Add(@"13;7999", "146373");
            program.SampleInputPartOne.Add(@"17;1104", "2764");
            program.SampleInputPartOne.Add(@"21;6111", "54718");
            program.SampleInputPartOne.Add(@"30;5807", "37305");

            program.Solve();
        }

        protected override string FindFirstSolution(string[] input)
        {
            long finalScore = GetWinningElfsScore(input);

            return finalScore.ToString();
        }

        protected override string FindSecondSolution(string[] input)
        {
            long finalScore = GetWinningElfsScore(input, 100);

            return finalScore.ToString();
        }

        private long GetWinningElfsScore(string[] input, int factor = 1)
        {
            string[] data = input[0].Split(";");
            int noOfPlayers = int.Parse(data[0]);
            int lastMarbleNumber = int.Parse(data[1]) * factor;

            Dictionary<int, long> score = new Dictionary<int, long>();
            LinkedList<long> play = new LinkedList<long>();

            int currentPlayer = 1;
            LinkedListNode<long> currentNode = new LinkedListNode<long>(-1);
            for (int i = 0; i <= lastMarbleNumber; i++)
            {
                if (i == 0 || i == 1)
                {
                    currentNode = new LinkedListNode<long>(i);
                    play.AddLast(currentNode);
                }
                else if (i % 23 == 0)
                {
                    AddScore(score, currentPlayer, i);
                    for (int c = 0; c < 6; c++)
                    {
                        currentNode = currentNode.Previous ?? currentNode.List.Last;
                    }

                    var nodeToRemove = currentNode.Previous ?? currentNode.List.Last;

                    AddScore(score, currentPlayer, nodeToRemove.Value);

                    play.Remove(nodeToRemove);
                }
                else
                {
                    var newNode = new LinkedListNode<long>(i);
                    play.AddAfter(currentNode.Next ?? currentNode.List.First, newNode);
                    currentNode = newNode;
                }

                currentPlayer++;
                if (currentPlayer > noOfPlayers)
                {
                    currentPlayer = 1;
                }
            }
            Console.WriteLine(currentPlayer--);
            long finalScore = score.Max(x => x.Value);
            return finalScore;
        }

        private void AddScore(Dictionary<int, long> score, int currentPlayer, long marbleNumber)
        {
            if (score.ContainsKey(currentPlayer))
            {
                score[currentPlayer] += marbleNumber;
            }
            else
            {
                score.Add(currentPlayer, marbleNumber);
            }
        }

        private int GoTwoStepsClockWise(int i, LinkedList<long> play)
        {
            i += 2;
            return i > play.Count() ? i - play.Count() : i;
        }

        private int GoSevenStepsCounterClockWise(int i, LinkedList<long> play)
        {
            i -= 7;
            if (i < 0)
            {
                i += play.Count();
            }

            return i;
        }
    }
}