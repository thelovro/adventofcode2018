﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Day22
{
    class Program : BaseAoc
    {
        static void Main(string[] args)
        {
            Program program = new Program();
            program.SampleInputPartOne.Add(@"depth: 510
target: 10,10", "114");

            program.SampleInputPartTwo.Add(@"depth: 510
target: 10,10", "45");

            program.Solve();
        }

        List<CaveRegion> regions;

        protected override string FindFirstSolution(string[] input)
        {
            (string[,] area, int targetX, int targetY) = DefineRegions(input);

            int counterRocky = 0;
            int counterWet = 0;
            int counterNarrow = 0;
            for (int y = 0; y <= targetY; y++)
            {
                for (int x = 0; x <= targetX; x++)
                {
                    if ((y, x) == (0, 0) || (y, x) == (targetY, targetX))
                    {
                        continue;
                    }

                    switch (area[y, x])
                    {
                        case CaveRegion.RegionType.Rocky:
                            counterRocky++;
                            break;
                        case CaveRegion.RegionType.Wet:
                            counterWet++;
                            break;
                        case CaveRegion.RegionType.Narrow:
                            counterNarrow++;
                            break;
                    }
                }
            }

            if (IsTest)
            {
                DrawArea(area);
            }

            return (counterRocky * 0 + counterWet * 1 + counterNarrow * 2).ToString();
        }

        protected override string FindSecondSolution(string[] input)
        {
            (string[,] area, int targetX, int targetY) = DefineRegions(input, 25);

            if (IsTest)
            {
                DrawArea(area);
            }

            //go get him!
            int timeTaken = GoGetHim(targetX, targetY);

            //yes, it takes quite long - 4m 30s
            return timeTaken.ToString();
        }

        private (string[,] area, int targetX, int targetY) DefineRegions(string[] input, int areaIncreament = 0)
        {
            int depth = int.Parse(input[0].Replace("depth: ", ""));
            int targetX = int.Parse(input[1].Replace("target: ", "").Split(",")[0]);
            int targetY = int.Parse(input[1].Replace("target: ", "").Split(",")[1]);
            CaveRegion initialCaveRegion = new CaveRegion() { X = 0, Y = 0, Depth = depth };

            regions = new List<CaveRegion>();

            int limitY = targetY + areaIncreament;
            int limitX = targetX + areaIncreament;

            for (int y = 0; y <= limitY; y++)
            {
                for (int x = 0; x <= limitX; x++)
                {
                    int geologicIndex = 0;
                    if (y == 0)
                    {
                        geologicIndex = x * 16807;
                    }
                    else if (x == 0)
                    {
                        geologicIndex = y * 48271;
                    }
                    else if (y == targetY && x == targetX)
                    {
                        geologicIndex = 0;
                    }
                    else
                    {
                        //Otherwise, the region's geologic index is the result of multiplying the erosion levels of the regions at X-1,Y and X,Y-1.
                        var region1 = regions.Where(r => r.Position == (x - 1, y)).First();
                        var region2 = regions.Where(r => r.Position == (x, y - 1)).First();
                        geologicIndex = region1.ErosionLevel * region2.ErosionLevel;
                    }

                    var caveRegion = new CaveRegion() { X = x, Y = y, Depth = depth, GeologicalIndex = geologicIndex };
                    regions.Add(caveRegion);
                }
            }

            string[,] area = new string[limitY + 1, limitX + 1];
            for (int y = 0; y <= limitY; y++)
            {
                for (int x = 0; x <= limitX; x++)
                {
                    string regionType = regions.Where(r => r.X == x && r.Y == y).First().Type;
                    area[y, x] = regionType;
                }
            }

            return (area, targetX, targetY);
        }

        private void DrawArea(string[,] area)
        {
            for (int j = 0; j <= area.GetUpperBound(0); j++)
            {
                for (int i = 0; i <= area.GetUpperBound(1); i++)
                {
                    Console.Write(area[j, i]);
                }

                Console.WriteLine();
            }
        }

        private int GoGetHim(int targetX, int targetY)
        {
            Queue<CaveMan> openList = new Queue<CaveMan>();
            HashSet<(int, int, CaveRegion.Tools)> seen = new HashSet<(int, int, CaveRegion.Tools)>();
            openList.Enqueue(new CaveMan() { X = 0, Y = 0, TimeTaken = 0, CurrentTool = CaveRegion.Tools.Torch });
            CaveMan currentCaveMan = null;

            while (openList.Count > 0)
            {
                currentCaveMan = openList.Dequeue();

                if (currentCaveMan.WaitPeriod > 0)
                {
                    if (!seen.Contains((currentCaveMan.X, currentCaveMan.Y, currentCaveMan.CurrentTool)))
                    {
                        openList.Enqueue(new CaveMan() { X = currentCaveMan.X, Y = currentCaveMan.Y, TimeTaken = currentCaveMan.TimeTaken + 1, WaitPeriod = currentCaveMan.WaitPeriod - 1, CurrentTool = currentCaveMan.CurrentTool });
                    }

                    continue;
                }

                seen.Add((currentCaveMan.X, currentCaveMan.Y, currentCaveMan.CurrentTool));

                if (currentCaveMan.X == targetX && currentCaveMan.Y == targetY)
                {
                    //we have to select a torch when we get to our guy!!
                    int additionalTime = currentCaveMan.CurrentTool == CaveRegion.Tools.Torch ? 0 : 7;
                    return currentCaveMan.TimeTaken + additionalTime;
                }

                var currentRegion = regions.Where(a => a.Position == currentCaveMan.Position).First();
                var regionAbove = regions.Where(a => a.Position == currentRegion.Above).FirstOrDefault();
                var regionLeft = regions.Where(a => a.Position == currentRegion.Left).FirstOrDefault();
                var regionRight = regions.Where(a => a.Position == currentRegion.Right).FirstOrDefault();
                var regionBelow = regions.Where(a => a.Position == currentRegion.Below).FirstOrDefault();

                if (regionAbove != null)
                {
                    EnqueueItems(openList, currentCaveMan, regionAbove, seen);
                }

                if (regionLeft != null)
                {
                    EnqueueItems(openList, currentCaveMan, regionLeft, seen);
                }

                if (regionRight != null)
                {
                    EnqueueItems(openList, currentCaveMan, regionRight, seen);
                }

                if (regionBelow != null)
                {
                    EnqueueItems(openList, currentCaveMan, regionBelow, seen);
                }
            }

            throw new Exception("not gonna happend");
        }

        private static void EnqueueItems(Queue<CaveMan> openList, CaveMan currentCaveMan, CaveRegion targetRegion, HashSet<(int, int, CaveRegion.Tools)> seen)
        {
            var tools = targetRegion.GetToolsForCurrentRegion();

            //if cave man doesn't have the right tool, change is needed
            if (!tools.Contains(currentCaveMan.CurrentTool))
            {
                if (!seen.Contains((targetRegion.X, targetRegion.Y, tools[0])))
                {
                    openList.Enqueue(new CaveMan() { X = targetRegion.X, Y = targetRegion.Y, TimeTaken = currentCaveMan.TimeTaken + 1, WaitPeriod = 7, CurrentTool = tools[0] });
                }
                if (!seen.Contains((targetRegion.X, targetRegion.Y, tools[1])))
                {
                    openList.Enqueue(new CaveMan() { X = targetRegion.X, Y = targetRegion.Y, TimeTaken = currentCaveMan.TimeTaken + 1, WaitPeriod = 7, CurrentTool = tools[1] });
                }
            }
            else
            {
                //hash set Add returns false if item is already added - and is faster than "Contains"
                if (seen.Add((targetRegion.X, targetRegion.Y, currentCaveMan.CurrentTool)))
                {
                    openList.Enqueue(new CaveMan() { X = targetRegion.X, Y = targetRegion.Y, TimeTaken = currentCaveMan.TimeTaken + 1, CurrentTool = currentCaveMan.CurrentTool });
                }
            }
        }
    }

    public class CaveMan
    {
        public CaveRegion.Tools CurrentTool { get; set; }
        public int TimeTaken { get; set; }
        public int WaitPeriod { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public (int, int) Position { get { return (X, Y); } }
    }

    public class CaveRegion
    {
        public enum Tools
        {
            Torch,
            ClimbingGear,
            Neither
        }

        public string[] GetType(Tools tool)
        {
            if (tool == Tools.Torch)
            {
                return new string[] { CaveRegion.RegionType.Rocky, CaveRegion.RegionType.Narrow };
            }
            else if (tool == Tools.ClimbingGear)
            {
                return new string[] { CaveRegion.RegionType.Rocky, CaveRegion.RegionType.Wet };
            }
            else if (tool == Tools.Neither)
            {
                return new string[] { CaveRegion.RegionType.Wet, CaveRegion.RegionType.Narrow };
            }

            throw new Exception("not gonna happend");
        }

        public Tools[] GetToolsForType(string currentType)
        {
            if (currentType == CaveRegion.RegionType.Rocky)
            {
                return new Tools[] { Tools.ClimbingGear, Tools.Torch };
            }
            else if (currentType == CaveRegion.RegionType.Narrow)
            {
                return new Tools[] { Tools.Neither, Tools.Torch };
            }
            else if (currentType == CaveRegion.RegionType.Wet)
            {
                return new Tools[] { Tools.ClimbingGear, Tools.Neither };
            }

            throw new Exception("not gonna happend");
        }

        public Tools[] GetToolsForCurrentRegion()
        {
            if (Type == CaveRegion.RegionType.Rocky)
            {
                return new Tools[] { Tools.ClimbingGear, Tools.Torch };
            }
            else if (Type == CaveRegion.RegionType.Narrow)
            {
                return new Tools[] { Tools.Neither, Tools.Torch };
            }
            else if (Type == CaveRegion.RegionType.Wet)
            {
                return new Tools[] { Tools.ClimbingGear, Tools.Neither };
            }

            throw new Exception("not gonna happend");
        }

        public class RegionType
        {
            public const string Rocky = ".";
            public const string Wet = "=";
            public const string Narrow = "|";
        }

        public string Type
        {
            get
            {
                if (ErosionLevel % 3 == 0)
                {
                    return RegionType.Rocky;
                }
                else if (ErosionLevel % 3 == 1)
                {
                    return RegionType.Wet;
                }
                else if (ErosionLevel % 3 == 2)
                {
                    return RegionType.Narrow;
                }
                else
                {
                    throw new Exception("not gonna happend");
                }
            }
        }
        public int ErosionLevel { get { return (GeologicalIndex + Depth) % 20183; } }
        public int GeologicalIndex { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public int Depth { get; set; }
        public (int, int) Position { get { return (X, Y); } }
        public (int, int) Above { get { return (X, Y - 1); } }
        public (int, int) Left { get { return (X - 1, Y); } }
        public (int, int) Right { get { return (X + 1, Y); } }
        public (int, int) Below { get { return (X, Y + 1); } }
    }
}