﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Day25
{
    class Program : BaseAoc
    {
        static void Main(string[] args)
        {
            Program program = new Program();
            program.SampleInputPartOne.Add(@" 0,0,0,0
 3,0,0,0
 0,3,0,0
 0,0,3,0
 0,0,0,3
 0,0,0,6
 9,0,0,0
12,0,0,0", "2");

            program.SampleInputPartOne.Add(@"-1,2,2,0
0,0,2,-2
0,0,0,-2
-1,2,0,0
-2,-2,-2,2
3,0,2,-1
-1,3,2,2
-1,0,-1,0
0,2,1,-2
3,0,0,0", "4");

            program.SampleInputPartOne.Add(@"1,-1,0,1
2,0,-1,0
3,2,-1,0
0,0,3,1
0,0,-1,-1
2,3,-2,0
-2,2,0,0
2,-2,0,-1
1,-1,0,-1
3,2,0,2", "3");

            program.SampleInputPartOne.Add(@"1,-1,-1,-2
-2,-2,0,1
0,2,1,3
-2,3,-2,1
0,2,3,-2
-1,-1,1,-2
0,-2,-1,0
-2,2,3,-1
1,2,2,0
-1,-2,0,-2", "8");

            program.Solve();
        }

        protected override string FindFirstSolution(string[] input)
        {
            List<(int, int, int, int)> positions = ParseInput(input);
            var constellations = GetConstellations(positions);

            return constellations.Count.ToString();
        }

        protected override string FindSecondSolution(string[] input)
        {
            //there is none
            return "";
        }

        private List<(int, int, int, int)> ParseInput(string[] input)
        {
            List<(int, int, int, int)> positions = new List<(int, int, int, int)>();
            foreach (var row in input)
            {
                var items = row.Replace(" ", "").Split(",");
                (int, int, int, int) newPosition = (int.Parse(items[0]), int.Parse(items[1]), int.Parse(items[2]), int.Parse(items[3]));
                positions.Add(newPosition);
            }

            return positions;
        }

        private Dictionary<int, List<(int, int, int, int)>> GetConstellations(List<(int, int, int, int)> positions)
        {
            Dictionary<int, List<(int, int, int, int)>> constellations = new Dictionary<int, List<(int, int, int, int)>>();
            List<(int, int, int, int)> seen = new List<(int, int, int, int)>();

            int counter = 0;
            while (seen.Count < positions.Count)
            {
                var newPosition = positions.First(p => !seen.Contains((p.Item1, p.Item2, p.Item3, p.Item4)));
                seen.Add(newPosition);
                List<(int, int, int, int)> positionsNearby = GetConstellationsRecursion(positions, seen, newPosition);

                constellations.Add(counter, new List<(int, int, int, int)>() { newPosition });
                constellations[counter].AddRange(positionsNearby);

                counter++;
            }

            return constellations;
        }

        private List<(int, int, int, int)> GetConstellationsRecursion(List<(int, int, int, int)> positions, List<(int, int, int, int)> seen, (int, int, int, int) newPosition)
        {
            List<(int, int, int, int)> constelattion = new List<(int, int, int, int)>();
            foreach (var position in positions.Where(p => !seen.Contains((p.Item1, p.Item2, p.Item3, p.Item4))))
            {
                if (Manhattan(position, newPosition) <= 3)
                {
                    constelattion.Add(position);
                    seen.Add(position);
                    constelattion.AddRange(GetConstellationsRecursion(positions, seen, position));
                }
            }

            return constelattion;
        }

        private int Manhattan((int, int, int, int) first, (int, int, int, int) second)
        {
            return Math.Abs(first.Item1 - second.Item1) + Math.Abs(first.Item2 - second.Item2) + Math.Abs(first.Item3 - second.Item3) + Math.Abs(first.Item4 - second.Item4);
        }
    }
}