﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Day11
{
    class Program : BaseAoc
    {
        static void Main(string[] args)
        {
            Program program = new Program();
            program.SampleInputPartOne.Add(@"18", "33,45");
            program.SampleInputPartOne.Add(@"42", "21,61");

            program.SampleInputPartTwo.Add(@"18", "90,269,16");
            program.SampleInputPartTwo.Add(@"42", "232,251,12");

            program.Solve();
        }

        protected override string FindFirstSolution(string[] input)
        {
            int serialNumber = int.Parse(input[0]);
            Dictionary<string, int> threeSquarePowerLevelList = new Dictionary<string, int>();

            for (int x = 1; x < 299; x++)
            {
                for (int y = 1; y < 299; y++)
                {
                    threeSquarePowerLevelList.Add(String.Format("{0},{1}", x, y), CalculatePower(x, y, serialNumber, 3));
                }
            }

            return threeSquarePowerLevelList.First(x => x.Value == threeSquarePowerLevelList.Max(y => y.Value)).Key;
        }

        protected override string FindSecondSolution(string[] input)
        {
            int serialNumber = int.Parse(input[0]);
            Dictionary<string, int> threeSquarePowerLevelList = new Dictionary<string, int>();
            int canvasSize = 300;

            Dictionary<string, int> maxPerSquareSize = new Dictionary<string, int>();
            int maxValue = 0;
            int counterMaxValue = 0;
            for (int squareSize = 1; squareSize <= canvasSize; squareSize++)
            {
                for (int x = 1; x < canvasSize - squareSize - 2; x++)
                {
                    for (int y = 1; y < canvasSize - squareSize - 2; y++)
                    {
                        threeSquarePowerLevelList.Add(String.Format("{0},{1},{2}", x, y, squareSize), CalculatePower(x, y, serialNumber, squareSize));
                    }
                }

                var maxItem = threeSquarePowerLevelList.Aggregate((l, r) => l.Value > r.Value ? l : r);
                maxPerSquareSize.Add(maxItem.Key, maxItem.Value);
                threeSquarePowerLevelList = new Dictionary<string, int>();

                if (maxItem.Value > maxValue)
                {
                    maxValue = maxItem.Value;
                    counterMaxValue = 0;
                }

                counterMaxValue++;

                if (counterMaxValue > 5)
                {
                    break;
                }
            }

            return maxPerSquareSize.Aggregate((l, r) => l.Value > r.Value ? l : r).Key;
        }

        Dictionary<string, int> cellValueCache = new Dictionary<string, int>();
        private int CalculatePower(int x, int y, int serialNumber, int squareSize)
        {
            int sumPower = 0;
            for (int i = x; i < x + squareSize; i++)
            {
                for (int j = y; j < y + squareSize; j++)
                {
                    string key = string.Format("{0},{1},{2}", i, j, serialNumber);
                    if (!cellValueCache.ContainsKey(key))
                    {
                        cellValueCache.Add(key, new PowerCell()
                        {
                            PostionX = i,
                            PostionY = j,
                            SerialNumber = serialNumber
                        }.PowerLevel);
                    }
                    sumPower += cellValueCache[key];
                }
            }

            return sumPower;
        }
    }

    public class PowerCell
    {
        public int PostionX { get; set; }
        public int PostionY { get; set; }
        public int SerialNumber { get; set; }
        public int RackID { get { return PostionX + 10; } }
        private int? _powerLevel;
        public int PowerLevel
        {
            get
            {
                if (_powerLevel == null)
                {
                    int power = RackID * PostionY;
                    power = power + SerialNumber;
                    power = power * RackID;

                    if (power.ToString().Length > 2)
                    {
                        int hundredsDigit = int.Parse(power.ToString().Substring(power.ToString().Length - 3, 1));
                        _powerLevel = hundredsDigit - 5;
                    }
                    else
                    {
                        _powerLevel = 0;
                    }
                }

                return _powerLevel.Value;
            }
        }
    }
}