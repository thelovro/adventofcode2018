﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Day21
{
    class Program : BaseAoc
    {
        static void Main(string[] args)
        {
            Program program = new Program();

            program.Solve();
        }

        protected override string FindFirstSolution(string[] input)
        {
            int[] register = new int[] { 0, 0, 0, 0, 0, 0 };
            int ipIndex = int.Parse(input[0].Replace("#ip ", ""));
            Dictionary<int, string> operations = GetOperationsList(input);
            return DoTheMagic(ref register, ipIndex, operations);
        }

        protected override string FindSecondSolution(string[] input)
        {
            int[] register = new int[] { 0, 0, 0, 0, 0, 0 };
            int ipIndex = int.Parse(input[0].Replace("#ip ", ""));
            Dictionary<int, string> operations = GetOperationsList(input);
            return DoTheMagic2(ref register, ipIndex, operations);
        }

        private string DoTheMagic(ref int[] register, int ipIndex, Dictionary<int, string> operations)
        {
            while (true)
            {
                string op = operations[register[ipIndex]];

                if (op.Split(" ")[0] == "addr")
                    register = Day16.Program.addr(op, register);
                else if (op.Split(" ")[0] == "addi")
                    register = Day16.Program.addi(op, register);
                else if (op.Split(" ")[0] == "mulr")
                    register = Day16.Program.mulr(op, register);
                else if (op.Split(" ")[0] == "muli")
                    register = Day16.Program.muli(op, register);
                else if (op.Split(" ")[0] == "banr")
                    register = Day16.Program.banr(op, register);
                else if (op.Split(" ")[0] == "bani")
                    register = Day16.Program.bani(op, register);
                else if (op.Split(" ")[0] == "borr")
                    register = Day16.Program.borr(op, register);
                else if (op.Split(" ")[0] == "bori")
                    register = Day16.Program.bori(op, register);
                else if (op.Split(" ")[0] == "setr")
                    register = Day16.Program.setr(op, register);
                else if (op.Split(" ")[0] == "seti")
                    register = Day16.Program.seti(op, register);
                else if (op.Split(" ")[0] == "gtir")
                    register = Day16.Program.gtir(op, register);
                else if (op.Split(" ")[0] == "gtri")
                    register = Day16.Program.gtri(op, register);
                else if (op.Split(" ")[0] == "gtrr")
                    register = Day16.Program.gtrr(op, register);
                else if (op.Split(" ")[0] == "eqir")
                    register = Day16.Program.eqir(op, register);
                else if (op.Split(" ")[0] == "eqri")
                    register = Day16.Program.eqri(op, register);
                else if (op.Split(" ")[0] == "eqrr")
                    register = Day16.Program.eqrr(op, register);
                else
                    throw new Exception("huh!!?");

                //only when value in register 0 is equal to value in register 3 => we try to find method number 31 which does not exist!!
                if (op == "eqrr 3 0 1")
                {
                    return register[3].ToString();
                }

                register[ipIndex] += 1;
            }

            throw new Exception("This should not happend!");
        }

        private string DoTheMagic2(ref int[] register, int ipIndex, Dictionary<int, string> operations)
        {
            List<int> magicValues = new List<int>();
            while (true)
            {
                string op = operations[register[ipIndex]];

                if (op.Split(" ")[0] == "addr")
                    register = Day16.Program.addr(op, register);
                else if (op.Split(" ")[0] == "addi")
                    register = Day16.Program.addi(op, register);
                else if (op.Split(" ")[0] == "mulr")
                    register = Day16.Program.mulr(op, register);
                else if (op.Split(" ")[0] == "muli")
                    register = Day16.Program.muli(op, register);
                else if (op.Split(" ")[0] == "banr")
                    register = Day16.Program.banr(op, register);
                else if (op.Split(" ")[0] == "bani")
                    register = Day16.Program.bani(op, register);
                else if (op.Split(" ")[0] == "borr")
                    register = Day16.Program.borr(op, register);
                else if (op.Split(" ")[0] == "bori")
                    register = Day16.Program.bori(op, register);
                else if (op.Split(" ")[0] == "setr")
                    register = Day16.Program.setr(op, register);
                else if (op.Split(" ")[0] == "seti")
                    register = Day16.Program.seti(op, register);
                else if (op.Split(" ")[0] == "gtir")
                    register = Day16.Program.gtir(op, register);
                else if (op.Split(" ")[0] == "gtri")
                    register = Day16.Program.gtri(op, register);
                else if (op.Split(" ")[0] == "gtrr")
                    register = Day16.Program.gtrr(op, register);
                else if (op.Split(" ")[0] == "eqir")
                    register = Day16.Program.eqir(op, register);
                else if (op.Split(" ")[0] == "eqri")
                    register = Day16.Program.eqri(op, register);
                else if (op.Split(" ")[0] == "eqrr")
                    register = Day16.Program.eqrr(op, register);
                else
                    throw new Exception("huh!!?");

                //only when value in register 0 is equal to value in register 3 => we try to find method number 31 which does not exist!!
                if (op == "eqrr 3 0 1")
                {
                    int newNumber = register[3];

                    if (!magicValues.Contains(newNumber))
                    {
                        magicValues.Add(newNumber);
                    }
                    else
                    {
                        //when we find a cycle - we return the last added magic value added to the list
                        //yes, yes, it lasts quite a while :) it takes >0,3s per cycle. magicValues contains 10310 items when first item (at ID 5866) is found
                        //but still parsing all that methods and finding all edge cases - there are too many :)
                        return magicValues[magicValues.Count - 1].ToString();
                    }
                }

                register[ipIndex] += 1;
            }

            throw new Exception("This should not happend!");
        }

        private static Dictionary<int, string> GetOperationsList(string[] input)
        {
            Dictionary<int, string> operations = new Dictionary<int, string>();
            for (int i = 1; i < input.Length; i++)
            {
                operations.Add(i - 1, input[i]);
            }

            return operations;
        }
    }
}