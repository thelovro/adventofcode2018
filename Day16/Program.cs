﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Day16
{
    public class Program : BaseAoc
    {
        static void Main(string[] args)
        {
            Program program = new Program();
            program.SampleInputPartOne.Add(@"Before: [3, 2, 1, 1]
9 2 1 2
After:  [3, 2, 2, 1]", "1");

            program.Solve();
        }

        protected override string FindFirstSolution(string[] input)
        {
            List<Sample> samples = ParseSamples(input);

            foreach (var sample in samples)
            {
                if (string.Join(", ", addr(sample.Operation, (int[])sample.Before.Clone())) == string.Join(", ", sample.After)) sample.NumberOfMatchinOpCodes++;
                if (string.Join(", ", addi(sample.Operation, (int[])sample.Before.Clone())) == string.Join(", ", sample.After)) sample.NumberOfMatchinOpCodes++;
                if (string.Join(", ", mulr(sample.Operation, (int[])sample.Before.Clone())) == string.Join(", ", sample.After)) sample.NumberOfMatchinOpCodes++;
                if (string.Join(", ", muli(sample.Operation, (int[])sample.Before.Clone())) == string.Join(", ", sample.After)) sample.NumberOfMatchinOpCodes++;
                if (string.Join(", ", banr(sample.Operation, (int[])sample.Before.Clone())) == string.Join(", ", sample.After)) sample.NumberOfMatchinOpCodes++;
                if (string.Join(", ", bani(sample.Operation, (int[])sample.Before.Clone())) == string.Join(", ", sample.After)) sample.NumberOfMatchinOpCodes++;
                if (string.Join(", ", borr(sample.Operation, (int[])sample.Before.Clone())) == string.Join(", ", sample.After)) sample.NumberOfMatchinOpCodes++;
                if (string.Join(", ", bori(sample.Operation, (int[])sample.Before.Clone())) == string.Join(", ", sample.After)) sample.NumberOfMatchinOpCodes++;
                if (string.Join(", ", setr(sample.Operation, (int[])sample.Before.Clone())) == string.Join(", ", sample.After)) sample.NumberOfMatchinOpCodes++;
                if (string.Join(", ", seti(sample.Operation, (int[])sample.Before.Clone())) == string.Join(", ", sample.After)) sample.NumberOfMatchinOpCodes++;
                if (string.Join(", ", gtir(sample.Operation, (int[])sample.Before.Clone())) == string.Join(", ", sample.After)) sample.NumberOfMatchinOpCodes++;
                if (string.Join(", ", gtri(sample.Operation, (int[])sample.Before.Clone())) == string.Join(", ", sample.After)) sample.NumberOfMatchinOpCodes++;
                if (string.Join(", ", gtrr(sample.Operation, (int[])sample.Before.Clone())) == string.Join(", ", sample.After)) sample.NumberOfMatchinOpCodes++;
                if (string.Join(", ", eqir(sample.Operation, (int[])sample.Before.Clone())) == string.Join(", ", sample.After)) sample.NumberOfMatchinOpCodes++;
                if (string.Join(", ", eqri(sample.Operation, (int[])sample.Before.Clone())) == string.Join(", ", sample.After)) sample.NumberOfMatchinOpCodes++;
                if (string.Join(", ", eqrr(sample.Operation, (int[])sample.Before.Clone())) == string.Join(", ", sample.After)) sample.NumberOfMatchinOpCodes++;
            }

            return samples.Where(x => x.NumberOfMatchinOpCodes >= 3).Count().ToString();
        }

        protected override string FindSecondSolution(string[] input)
        {
            List<Sample> samples = ParseSamples(input);

            //determine all possible operations
            DetermineAllPossibleOperations(samples);

            //determine which operation is meant by which ID
            Dictionary<string, string> operations = MatchIdsWithActualOperations(samples);

            //run input operations through code and find final solution
            int[] register = RunInputThroughOperations(input, operations);

            return register[0].ToString();
        }

        private List<Sample> ParseSamples(string[] input)
        {
            List<Sample> samples = new List<Sample>();

            for (int i = 0; i < input.Length; i++)
            {
                if (!input[i].StartsWith("Before"))
                {
                    break;
                }
                samples.Add(new Sample()
                {
                    Before = input[i++].Replace("Before: ", "").TrimStart('[').TrimEnd(']').Split(", ").Select(x => int.Parse(x)).ToArray(),
                    Operation = input[i++],
                    After = input[i++].Replace("After:  ", "").TrimStart('[').TrimEnd(']').Split(", ").Select(x => int.Parse(x)).ToArray(),
                    NumberOfMatchinOpCodes = 0
                });
            }

            return samples;
        }

        private List<string> ParseSamples2(string[] input)
        {
            List<string> operations = new List<string>();
            for (int i = 3342; i < input.Length; i++)
            {
                operations.Add(input[i]);
            }

            return operations;
        }

        private static void DetermineAllPossibleOperations(List<Sample> samples)
        {
            foreach (var sample in samples)
            {
                if (string.Join(", ", addr(sample.Operation, (int[])sample.Before.Clone())) == string.Join(", ", sample.After))
                {
                    sample.NumberOfMatchinOpCodes++;
                    sample.ListOfOperations.Add("addr");
                }
                if (string.Join(", ", addi(sample.Operation, (int[])sample.Before.Clone())) == string.Join(", ", sample.After))
                {
                    sample.NumberOfMatchinOpCodes++;
                    sample.ListOfOperations.Add("addi");
                }
                if (string.Join(", ", mulr(sample.Operation, (int[])sample.Before.Clone())) == string.Join(", ", sample.After))
                {
                    sample.NumberOfMatchinOpCodes++;
                    sample.ListOfOperations.Add("mulr");
                }
                if (string.Join(", ", muli(sample.Operation, (int[])sample.Before.Clone())) == string.Join(", ", sample.After))
                {
                    sample.NumberOfMatchinOpCodes++;
                    sample.ListOfOperations.Add("muli");
                }
                if (string.Join(", ", banr(sample.Operation, (int[])sample.Before.Clone())) == string.Join(", ", sample.After))
                {
                    sample.NumberOfMatchinOpCodes++;
                    sample.ListOfOperations.Add("banr");
                }
                if (string.Join(", ", bani(sample.Operation, (int[])sample.Before.Clone())) == string.Join(", ", sample.After))
                {
                    sample.NumberOfMatchinOpCodes++;
                    sample.ListOfOperations.Add("bani");
                }
                if (string.Join(", ", borr(sample.Operation, (int[])sample.Before.Clone())) == string.Join(", ", sample.After))
                {
                    sample.NumberOfMatchinOpCodes++;
                    sample.ListOfOperations.Add("borr");
                }
                if (string.Join(", ", bori(sample.Operation, (int[])sample.Before.Clone())) == string.Join(", ", sample.After))
                {
                    sample.NumberOfMatchinOpCodes++;
                    sample.ListOfOperations.Add("bori");
                }
                if (string.Join(", ", setr(sample.Operation, (int[])sample.Before.Clone())) == string.Join(", ", sample.After))
                {
                    sample.NumberOfMatchinOpCodes++;
                    sample.ListOfOperations.Add("setr");
                }
                if (string.Join(", ", seti(sample.Operation, (int[])sample.Before.Clone())) == string.Join(", ", sample.After))
                {
                    sample.NumberOfMatchinOpCodes++;
                    sample.ListOfOperations.Add("seti");
                }
                if (string.Join(", ", gtir(sample.Operation, (int[])sample.Before.Clone())) == string.Join(", ", sample.After))
                {
                    sample.NumberOfMatchinOpCodes++;
                    sample.ListOfOperations.Add("gtir");
                }
                if (string.Join(", ", gtri(sample.Operation, (int[])sample.Before.Clone())) == string.Join(", ", sample.After))
                {
                    sample.NumberOfMatchinOpCodes++;
                    sample.ListOfOperations.Add("gtri");
                }
                if (string.Join(", ", gtrr(sample.Operation, (int[])sample.Before.Clone())) == string.Join(", ", sample.After))
                {
                    sample.NumberOfMatchinOpCodes++;
                    sample.ListOfOperations.Add("gtrr");
                }
                if (string.Join(", ", eqir(sample.Operation, (int[])sample.Before.Clone())) == string.Join(", ", sample.After))
                {
                    sample.NumberOfMatchinOpCodes++;
                    sample.ListOfOperations.Add("eqir");
                }
                if (string.Join(", ", eqri(sample.Operation, (int[])sample.Before.Clone())) == string.Join(", ", sample.After))
                {
                    sample.NumberOfMatchinOpCodes++;
                    sample.ListOfOperations.Add("eqri");
                }
                if (string.Join(", ", eqrr(sample.Operation, (int[])sample.Before.Clone())) == string.Join(", ", sample.After))
                {
                    sample.NumberOfMatchinOpCodes++;
                    sample.ListOfOperations.Add("eqrr");
                }
            }
        }

        private static Dictionary<string, string> MatchIdsWithActualOperations(List<Sample> samples)
        {
            Dictionary<string, string> operations = new Dictionary<string, string>();
            while (samples.Any(x => x.ListOfOperations.Count() > 0))
            {
                var sample = samples.First(x => x.ListOfOperations.Count() == 1);
                string operatonName = sample.ListOfOperations[0];
                string operationCode = sample.Operation.Split(" ")[0];
                operations.Add(operationCode, operatonName);

                foreach (var s in samples)
                {
                    if (s.ListOfOperations.Contains(operatonName))
                    {
                        s.ListOfOperations.Remove(operatonName);
                    }
                }
            }

            return operations;
        }

        private int[] RunInputThroughOperations(string[] input, Dictionary<string, string> operations)
        {
            List<string> inputOperations = ParseSamples2(input);
            int[] register = new int[] { 0, 0, 0, 0 };
            foreach (var op in inputOperations)
            {
                if (operations[op.Split(" ")[0]] == "addr")
                    register = addr(op, register);
                else if (operations[op.Split(" ")[0]] == "addi")
                    register = addi(op, register);
                else if (operations[op.Split(" ")[0]] == "mulr")
                    register = mulr(op, register);
                else if (operations[op.Split(" ")[0]] == "muli")
                    register = muli(op, register);
                else if (operations[op.Split(" ")[0]] == "banr")
                    register = banr(op, register);
                else if (operations[op.Split(" ")[0]] == "bani")
                    register = bani(op, register);
                else if (operations[op.Split(" ")[0]] == "borr")
                    register = borr(op, register);
                else if (operations[op.Split(" ")[0]] == "bori")
                    register = bori(op, register);
                else if (operations[op.Split(" ")[0]] == "setr")
                    register = setr(op, register);
                else if (operations[op.Split(" ")[0]] == "seti")
                    register = seti(op, register);
                else if (operations[op.Split(" ")[0]] == "gtir")
                    register = gtir(op, register);
                else if (operations[op.Split(" ")[0]] == "gtri")
                    register = gtri(op, register);
                else if (operations[op.Split(" ")[0]] == "gtrr")
                    register = gtrr(op, register);
                else if (operations[op.Split(" ")[0]] == "eqir")
                    register = eqir(op, register);
                else if (operations[op.Split(" ")[0]] == "eqri")
                    register = eqri(op, register);
                else if (operations[op.Split(" ")[0]] == "eqrr")
                    register = eqrr(op, register);
                else
                    throw new Exception("huh!!?");
            }

            return register;
        }

        #region Operations
        //addr(add register) stores into register C the result of adding register A and register B.
        public static int[] addr(string operation, int[] registers)
        {
            //parse data
            var data = operation.Split(" ");
            int regA = int.Parse(data[1]);
            int regB = int.Parse(data[2]);
            int regC = int.Parse(data[3]);

            //override with result
            registers[regC] = registers[regA] + registers[regB];

            return registers;
        }

        //addi(add immediate) stores into register C the result of adding register A and value B.
        public static int[] addi(string operation, int[] registers)
        {
            //parse data
            var data = operation.Split(" ");
            int regA = int.Parse(data[1]);
            int regB = int.Parse(data[2]);
            int regC = int.Parse(data[3]);

            //override with result
            registers[regC] = registers[regA] + regB;

            return registers;
        }

        //Multiplication:
        //mulr (multiply register) stores into register C the result of multiplying register A and register B.
        public static int[] mulr(string operation, int[] registers)
        {
            //parse data
            var data = operation.Split(" ");
            int regA = int.Parse(data[1]);
            int regB = int.Parse(data[2]);
            int regC = int.Parse(data[3]);

            //override with result
            registers[regC] = registers[regA] * registers[regB];

            return registers;
        }

        //muli (multiply immediate) stores into register C the result of multiplying register A and value B.
        public static int[] muli(string operation, int[] registers)
        {
            //parse data
            var data = operation.Split(" ");
            int regA = int.Parse(data[1]);
            int regB = int.Parse(data[2]);
            int regC = int.Parse(data[3]);

            //override with result
            registers[regC] = registers[regA] * regB;

            return registers;
        }

        //Bitwise AND:
        //banr (bitwise AND register) stores into register C the result of the bitwise AND of register A and register B.
        public static int[] banr(string operation, int[] registers)
        {
            //parse data
            var data = operation.Split(" ");
            int regA = int.Parse(data[1]);
            int regB = int.Parse(data[2]);
            int regC = int.Parse(data[3]);

            //override with result
            registers[regC] = registers[regA] & registers[regB];

            return registers;
        }
        //bani(bitwise AND immediate) stores into register C the result of the bitwise AND of register A and value B.
        public static int[] bani(string operation, int[] registers)
        {
            //parse data
            var data = operation.Split(" ");
            int regA = int.Parse(data[1]);
            int regB = int.Parse(data[2]);
            int regC = int.Parse(data[3]);

            //override with result
            registers[regC] = registers[regA] & regB;

            return registers;
        }

        //Bitwise OR:
        //borr (bitwise OR register) stores into register C the result of the bitwise OR of register A and register B.
        public static int[] borr(string operation, int[] registers)
        {
            //parse data
            var data = operation.Split(" ");
            int regA = int.Parse(data[1]);
            int regB = int.Parse(data[2]);
            int regC = int.Parse(data[3]);

            //override with result
            registers[regC] = registers[regA] | registers[regB];

            return registers;
        }
        //bori(bitwise OR immediate) stores into register C the result of the bitwise OR of register A and value B.
        public static int[] bori(string operation, int[] registers)
        {
            //parse data
            var data = operation.Split(" ");
            int regA = int.Parse(data[1]);
            int regB = int.Parse(data[2]);
            int regC = int.Parse(data[3]);

            //override with result
            registers[regC] = registers[regA] | regB;

            return registers;
        }

        //Assignment:
        //setr (set register) copies the contents of register A into register C. (Input B is ignored.)
        public static int[] setr(string operation, int[] registers)
        {
            //parse data
            var data = operation.Split(" ");
            int regA = int.Parse(data[1]);
            int regB = int.Parse(data[2]);
            int regC = int.Parse(data[3]);

            //override with result
            registers[regC] = registers[regA];

            return registers;
        }
        //seti(set immediate) stores value A into register C. (Input B is ignored.)
        public static int[] seti(string operation, int[] registers)
        {
            //parse data
            var data = operation.Split(" ");
            int regA = int.Parse(data[1]);
            int regB = int.Parse(data[2]);
            int regC = int.Parse(data[3]);

            //override with result
            registers[regC] = regA;

            return registers;
        }

        //Greater-than testing:
        //gtir(greater-than immediate/register) sets register C to 1 if value A is greater than register B.Otherwise, register C is set to 0.
        public static int[] gtir(string operation, int[] registers)
        {
            //parse data
            var data = operation.Split(" ");
            int regA = int.Parse(data[1]);
            int regB = int.Parse(data[2]);
            int regC = int.Parse(data[3]);

            //override with result
            registers[regC] = regA > registers[regB] ? 1 : 0;

            return registers;
        }

        //gtri (greater-than register/immediate) sets register C to 1 if register A is greater than value B.Otherwise, register C is set to 0.
        public static int[] gtri(string operation, int[] registers)
        {
            //parse data
            var data = operation.Split(" ");
            int regA = int.Parse(data[1]);
            int regB = int.Parse(data[2]);
            int regC = int.Parse(data[3]);

            //override with result
            registers[regC] = registers[regA] > regB ? 1 : 0;

            return registers;
        }

        //gtrr (greater-than register/register) sets register C to 1 if register A is greater than register B.Otherwise, register C is set to 0.
        public static int[] gtrr(string operation, int[] registers)
        {
            //parse data
            var data = operation.Split(" ");
            int regA = int.Parse(data[1]);
            int regB = int.Parse(data[2]);
            int regC = int.Parse(data[3]);

            //override with result
            registers[regC] = registers[regA] > registers[regB] ? 1 : 0;

            return registers;
        }


        //Equality testing:
        //eqir (equal immediate/register) sets register C to 1 if value A is equal to register B.Otherwise, register C is set to 0.
        public static int[] eqir(string operation, int[] registers)
        {
            //parse data
            var data = operation.Split(" ");
            int regA = int.Parse(data[1]);
            int regB = int.Parse(data[2]);
            int regC = int.Parse(data[3]);

            //override with result
            registers[regC] = regA == registers[regB] ? 1 : 0;

            return registers;
        }

        //eqri (equal register/immediate) sets register C to 1 if register A is equal to value B.Otherwise, register C is set to 0.
        public static int[] eqri(string operation, int[] registers)
        {
            //parse data
            var data = operation.Split(" ");
            int regA = int.Parse(data[1]);
            int regB = int.Parse(data[2]);
            int regC = int.Parse(data[3]);

            //override with result
            registers[regC] = registers[regA] == regB ? 1 : 0;

            return registers;
        }

        //eqrr (equal register/register) sets register C to 1 if register A is equal to register B.Otherwise, register C is set to 0.
        public static int[] eqrr(string operation, int[] registers)
        {
            //parse data
            var data = operation.Split(" ");
            int regA = int.Parse(data[1]);
            int regB = int.Parse(data[2]);
            int regC = int.Parse(data[3]);

            //override with result
            registers[regC] = registers[regA] == registers[regB] ? 1 : 0;

            return registers;
        }
        #endregion Operations
    }

    public class Sample
    {
        public int[] Before { get; set; }
        public int[] After { get; set; }
        public string Operation { get; set; }
        public int NumberOfMatchinOpCodes { get; set; }
        private List<string> _listOfOperations;
        public List<string> ListOfOperations
        {
            get
            {
                if (_listOfOperations == null)
                {
                    _listOfOperations = new List<string>();
                }

                return _listOfOperations;
            }
        }
    }
}