﻿using Common;
using System;
using System.Linq;
using System.Collections.Generic;

namespace Day05
{
    class Program : BaseAoc
    {
        static void Main(string[] args)
        {
            Program program = new Program();
            program.SampleInputPartOne.Add(@"dabAcCaCBAcCcaDA", "10");

            program.SampleInputPartTwo.Add(@"dabAcCaCBAcCcaDA", "4");

            program.Solve();
        }

        protected override string FindFirstSolution(string[] input)
        {
            return ShortenTheString(input[0]).ToString();
        }

        private int ShortenTheString(string work)
        {
            int listLength = work.Length + 1;
            while (work.Length < listLength)
            {
                listLength = work.Length;
                for (int i = work.Length - 1; i > 0; i--)
                {
                    if (work[i].ToString().ToUpper() == work[i - 1].ToString().ToUpper()
                        && (char.IsLower(work[i]) && char.IsUpper(work[i - 1]) || char.IsUpper(work[i]) && char.IsLower(work[i - 1])))
                    {
                        work = work.Remove(i, 1);
                        work = work.Remove(i - 1, 1);
                        i--;
                    }
                }
            }

            return listLength;
        }

        protected override string FindSecondSolution(string[] input)
        {
            string work = input[0];
            int shortest = work.Length;
            string workLower = work.ToLower();

            IEnumerable<char> distinctItems = workLower.ToCharArray().Distinct();

            foreach (char c in distinctItems)
            {
                string tmpWork;
                tmpWork = work.Replace(c.ToString(), "");
                tmpWork = tmpWork.Replace(c.ToString().ToUpper(), "");

                int result = ShortenTheString(tmpWork);

                if (result < shortest)
                {
                    shortest = result;
                }
            }

            return shortest.ToString();
        }
    }
}