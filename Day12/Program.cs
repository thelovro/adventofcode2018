﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Day12
{
    class Program : BaseAoc
    {
        static void Main(string[] args)
        {
            Program program = new Program();
            program.SampleInputPartOne.Add(@"initial state: #..#.#..##......###...###

...## => #
..#.. => #
.#... => #
.#.#. => #
.#.## => #
.##.. => #
.#### => #
#.#.# => #
#.### => #
##.#. => #
##.## => #
###.. => #
###.# => #
####. => #", "325");

            program.Solve();
        }

        protected override string FindFirstSolution(string[] input)
        {
            string initialState = "....." + input[0].Replace("initial state: ", "") + "...............";

            List<string> planProducingCombinations = new List<string>();
            for (int i = 2; i < input.Length; i++)
            {
                if (input[i].EndsWith("#"))
                {
                    planProducingCombinations.Add(input[i].Split(" ")[0]);
                }
            }

            List<string> state = new List<string>(initialState.Select(c => c.ToString()).ToList()); ;
            for (int x = 0; x < 20; x++)
            {
                List<string> newState = state.ToList();
                for (int i = 2; i < state.Count() - 2; i++)
                {
                    string combination = state[i - 2] + state[i - 1] + state[i] + state[i + 1] + state[i + 2];
                    newState[i] = planProducingCombinations.Contains(combination) ? "#" : ".";
                }

                state = newState;
            }

            int result = FindAllPlantContainingPot(state);
            return result.ToString();
        }

        protected override string FindSecondSolution(string[] input)
        {
            string initialState = "....." + input[0].Replace("initial state: ", "") + "................";

            List<string> planProducingCombinations = new List<string>();
            for (int i = 2; i < input.Length; i++)
            {
                if (input[i].EndsWith("#"))
                {
                    planProducingCombinations.Add(input[i].Split(" ")[0]);
                }
            }

            List<string> state = new List<string>(initialState.Select(c => c.ToString()).ToList());
            long finalResult = 0;
            int growthCounter = 0;
            for (Int64 x = 0; x < 50000000000; x++)
            {
                state.AddRange("..".Select(c => c.ToString()).ToList());
                List<string> newState = state.ToList();
                for (int i = 2; i < state.Count() - 2; i++)
                {
                    string combination = state[i - 2] + state[i - 1] + state[i] + state[i + 1] + state[i + 2];
                    newState[i] = planProducingCombinations.Contains(combination) ? "#" : ".";
                }

                int prev = state.Where(c => c == "#").Count();
                int current = newState.Where(c => c == "#").Count();

                int lastGrowth = prev - current;
                if (lastGrowth == 0)
                {
                    growthCounter++;
                }
                else
                {
                    growthCounter = 0;
                }

                if (growthCounter > 10)
                {
                    int resultPrev = FindAllPlantContainingPot(state);
                    int resultCurrent = FindAllPlantContainingPot(newState);

                    int diff = resultCurrent - resultPrev;

                    finalResult = (50000000000 - x - 1) * diff + resultCurrent;

                    break;
                }

                state = newState;
            }

            return finalResult.ToString();
        }

        private int FindAllPlantContainingPot(List<string> state)
        {
            int sum = 0;
            for (int i = 0; i < state.Count(); i++)
            {
                if (state.ElementAt(i) == "#")
                {
                    sum += i - 5;
                }
            }

            return sum;
        }
    }
}