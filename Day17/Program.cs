﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Day17
{
    class Program : BaseAoc
    {
        static void Main(string[] args)
        {
            Program program = new Program();
            program.SampleInputPartOne.Add(@"x=495, y=2..7
y=7, x=495..501
x=501, y=3..7
x=498, y=2..4
x=506, y=1..2
x=498, y=10..13
x=504, y=10..13
y=13, x=498..504", "57");

            program.SampleInputPartOne.Add(@"y=1, x=499..501
y=5, x=496..504
x=496, y=4..5
x=504, y=3..5", "22");

            program.SampleInputPartOne.Add(@"y=1, x=499..501
y=5, x=497..504
x=497, y=4..5
x=504, y=4..5
y=8, x=495..498
y=8, x=501..506
x=495, y=7..8
x=498, y=6..8
x=501, y=5..8
x=506, y=6..8
y=12, x=490..510", "73");

            program.SampleInputPartOne.Add(@"x=507, y=1..2
y=10, x=495..505
x=495, y=3..10
x=505, y=3..10
y=7, x=500..502
x=500, y=5..7
x=502, y=5..7", "86");

            program.Solve();
        }

        int SourceOfWaterX = 0;
        int SourceOfWaterY = 0;
        int MaximumDepth = 0;
        int StartY = 0;

        protected override string FindFirstSolution(string[] input)
        {
            string[,] map = ParseVeins(input);

            if (IsTest)
            {
                DrawMap(map);
            }

            map = LetTheWaterFlow(map);

            if (IsTest)
            {
                DrawMap(map);
            }

            return CountWater(map).ToString();
        }

        protected override string FindSecondSolution(string[] input)
        {
            string[,] map = ParseVeins(input);

            if (IsTest)
            {
                DrawMap(map);
            }

            map = LetTheWaterFlow(map);

            if (IsTest)
            {
                DrawMap(map);
            }

            return CountStillWater(map).ToString();
        }

        private string[,] LetTheWaterFlow(string[,] map)
        {
            string[] bottom = new string[] { "#", "~" };

            Queue<WaterSource> waterSources = new Queue<WaterSource>();
            waterSources.Enqueue(new WaterSource { X = SourceOfWaterX, Y = SourceOfWaterY, Parent = null });

            while (waterSources.Count() > 0)
            {
                WaterSource current = waterSources.Dequeue();

                if (!bottom.Contains(map[current.Y + 1, current.X]))
                {
                    if (map[current.Y + 1, current.X] == "|")
                    {
                        continue;
                    }
                    map[current.Y + 1, current.X] = "|";
                    if (current.Y + 1 < MaximumDepth)
                    {
                        waterSources.Enqueue(new WaterSource { X = current.X, Y = current.Y + 1, Parent = current });
                    }
                }
                else if (map[current.Y, current.X] == "~")
                {
                    //some else source filled the pool already
                    continue;
                }
                else
                {
                    //we reached the bottom
                    //check left and right to see if it is a pool, or a water will start flowing away
                    (int indexLeft, int indexright, bool isOnLeftWaterfall, bool isOnRightWaterfall) = DetermineBottomType(map, current, waterSources);
                    for (int i = indexLeft; i <= indexright; i++)
                    {
                        map[current.Y, i] = (isOnLeftWaterfall || isOnRightWaterfall ? "|" : "~");
                    }

                    if (!isOnLeftWaterfall && !isOnRightWaterfall)
                    {
                        //we simply move up!
                        while (current.Parent.Y >= current.Y)
                        {
                            current = current.Parent;
                        }

                        waterSources.Enqueue(current.Parent);
                    }
                    else
                    {
                        if (isOnLeftWaterfall)
                        {
                            var a = new WaterSource { X = indexLeft, Y = current.Y, Parent = current };
                            waterSources.Enqueue(a);
                            while (map[a.Y + 1, a.X] == "|")
                            {
                                a = new WaterSource { X = indexLeft, Y = a.Y + 1, Parent = a };
                                waterSources.Enqueue(a);
                            }
                        }
                        if (isOnRightWaterfall)
                        {
                            var a = new WaterSource { X = indexright, Y = current.Y, Parent = current };
                            waterSources.Enqueue(a);
                            while (map[a.Y + 1, a.X] == "|")
                            {
                                a = new WaterSource { X = indexright, Y = a.Y + 1, Parent = a };
                                waterSources.Enqueue(a);
                            }
                        }
                    }
                }
            }

            return map;
        }

        private (int indexLeft, int indexRight, bool isOnLeftWaterfall, bool isRightWaterfall) DetermineBottomType(string[,] map, WaterSource current, Queue<WaterSource> waterSources)
        {
            int indexLeft = current.X;
            int indexRight = current.X;
            bool isOnLeftWaterfall = false;
            bool isOnRightWaterfall = false;

            //left side
            while (true)
            {
                if (string.IsNullOrEmpty(map[current.Y + 1, indexLeft]) || map[current.Y + 1, indexLeft] == "|")
                {
                    //waterfall!!
                    isOnLeftWaterfall = true;
                    break;
                }
                else if (map[current.Y, indexLeft - 1] == "#")
                {
                    //pool
                    break;
                }
                indexLeft--;
            }

            //right side
            while (true)
            {
                if (map[current.Y + 1, indexRight] == "|")
                {

                }
                if (string.IsNullOrEmpty(map[current.Y + 1, indexRight]) || map[current.Y + 1, indexRight] == "|")
                {
                    //waterfall!!
                    isOnRightWaterfall = true;
                    break;
                }
                else if (map[current.Y, indexRight + 1] == "#")
                {
                    //pool
                    break;
                }
                indexRight++;
            }

            return (indexLeft, indexRight, isOnLeftWaterfall, isOnRightWaterfall);
        }

        private int CountWater(string[,] map)
        {
            int counter = 0;
            for (int y = StartY; y <= map.GetUpperBound(0); y++)
            {
                for (int x = 0; x <= map.GetUpperBound(1); x++)
                {
                    if (map[y, x] == "~" || map[y, x] == "|")
                    {
                        counter++;
                    }
                }
            }

            return counter;
        }

        private int CountStillWater(string[,] map)
        {
            int counter = 0;
            for (int y = StartY; y <= map.GetUpperBound(0); y++)
            {
                for (int x = 0; x <= map.GetUpperBound(1); x++)
                {
                    if (map[y, x] == "~")
                    {
                        counter++;
                    }
                }
            }

            return counter;
        }

        private void DrawMap(string[,] map, int minLimit = -1, int maxLimit = int.MaxValue)
        {
            for (int y = 0; y <= map.GetUpperBound(0); y++)
            {
                if (y < minLimit)
                {
                    continue;
                }
                for (int x = 0; x <= map.GetUpperBound(1); x++)
                {
                    if (y == SourceOfWaterY && x == SourceOfWaterX)
                    {
                        Console.Write("+");
                    }
                    else
                    {
                        Console.Write(string.IsNullOrEmpty(map[y, x]) ? "." : map[y, x]);
                    }
                }

                if (maxLimit < y)
                    break;

                Console.WriteLine();
            }

            Console.WriteLine();
        }

        private string[,] ParseVeins(string[] input)
        {
            int minX = 0;
            int maxX = 0;
            int minY = 0;
            int maxY = 0;

            //find min and max values first:
            minX = Math.Min(
                input.Where(x => x.StartsWith("x")).Min(x => int.Parse(x.Split(", ")[0].Replace("x=", ""))),
                input.Where(x => x.StartsWith("y")).Min(x => int.Parse(x.Split(", ")[1].Replace("x=", "").Substring(0, x.Split(", ")[1].Replace("x=", "").IndexOf(".")))));
            maxX = Math.Max(
                input.Where(x => x.StartsWith("x")).Max(x => int.Parse(x.Split(", ")[0].Replace("x=", ""))),
                input.Where(x => x.StartsWith("y")).Max(x => int.Parse(x.Split(", ")[1].Substring(x.Split(", ")[1].LastIndexOf(".") + 1, x.Split(", ")[1].Length - x.Split(", ")[1].LastIndexOf(".") - 1))));

            minY = Math.Min(
                input.Where(x => x.StartsWith("y")).Min(x => int.Parse(x.Split(", ")[0].Replace("y=", ""))),
                input.Where(x => x.StartsWith("x")).Min(x => int.Parse(x.Split(", ")[1].Replace("y=", "").Substring(0, x.Split(", ")[1].Replace("y=", "").IndexOf(".")))));
            maxY = Math.Max(
                input.Where(x => x.StartsWith("y")).Max(x => int.Parse(x.Split(", ")[0].Replace("y=", ""))),
                input.Where(x => x.StartsWith("x")).Max(x => int.Parse(x.Split(", ")[1].Substring(x.Split(", ")[1].LastIndexOf(".") + 1, x.Split(", ")[1].Length - x.Split(", ")[1].LastIndexOf(".") - 1))));

            SourceOfWaterX = 500 - minX + 1;
            MaximumDepth = maxY;
            StartY = minY;

            string[,] map = new string[maxY + 1, maxX - minX + 3];

            foreach (var row in input)
            {
                List<int> distance = new List<int>();
                List<int> depth = new List<int>();

                if (row.StartsWith("x"))
                {
                    distance.Add(int.Parse(row.Split(", ")[0].Replace("x=", "")) - minX + 1);
                    string tmp = row.Split(", ")[1].Replace("y=", "");
                    int start = int.Parse(tmp.Substring(0, tmp.IndexOf(".")));
                    int end = int.Parse(tmp.Substring(tmp.LastIndexOf(".") + 1, tmp.Length - tmp.LastIndexOf(".") - 1));
                    for (int d = start; d <= end; d++)
                    {
                        depth.Add(d);
                    }
                }
                else
                {
                    depth.Add(int.Parse(row.Split(", ")[0].Replace("y=", "")));
                    string tmp = row.Split(", ")[1].Replace("x=", "");
                    int start = int.Parse(tmp.Substring(0, tmp.IndexOf(".")));
                    int end = int.Parse(tmp.Substring(tmp.LastIndexOf(".") + 1, tmp.Length - tmp.LastIndexOf(".") - 1));
                    for (int d = start; d < end; d++)
                    {
                        distance.Add(d - minX + 2);
                    }
                }

                foreach (var de in depth)
                {
                    foreach (var di in distance)
                    {
                        map[de, di] = "#";
                    }
                }
            }

            return map;
        }
    }

    public class WaterSource
    {
        public int X;
        public int Y;
        public (int, int) Position { get { return (X, Y); } }
        public (int, int) PositionBelow { get { return (X, Y - 1); } }
        public WaterSource Parent;
    }
}