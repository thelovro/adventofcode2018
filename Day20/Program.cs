﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Day20
{
    class Program : BaseAoc
    {
        static void Main(string[] args)
        {
            Program program = new Program();
            program.SampleInputPartOne.Add(@"^WNE$", "3");
            program.SampleInputPartOne.Add(@"^ENWWW(NEEE|SSE(EE|N))$", "10");
            program.SampleInputPartOne.Add(@"^ENNWSWW(NEWS|)SSSEEN(WNSE|)EE(SWEN|)NNN$", "18");
            program.SampleInputPartOne.Add(@"^ESSWWN(E|NNENN(EESS(WNSE|)SSS|WWWSSSSE(SW|NNNE)))$", "23");
            program.SampleInputPartOne.Add(@"^WSSEESWWWNW(S|NENNEEEENN(ESSSSW(NWSW|SSEN)|WSWWN(E|WWS(E|SS))))$", "31");
            program.SampleInputPartOne.Add(@"^N(EEENWWW|N)$", "5");
            program.SampleInputPartOne.Add(@"^N(EEENSWWW|)N$", "5");
            program.SampleInputPartOne.Add(@"^(SENNWWSWN|WSW)$", "4");

            program.Solve();
        }

        System.Collections.Stack myStack;
        List<Area> areas;

        protected override string FindFirstSolution(string[] input)
        {
            List<Area> distances = new List<Area>();
            DoTheMagic(input, distances);

            int length = distances.Select(d => d.Distance).Max();

            return length.ToString();
        }

        protected override string FindSecondSolution(string[] input)
        {
            List<Area> distances = new List<Area>();
            DoTheMagic(input, distances);

            int numberOfDistantRooms = distances.Where(d => d.Distance >= 1000).Count();

            return numberOfDistantRooms.ToString();
        }

        private void DoTheMagic(string[] input, List<Area> distances)
        {
            myStack = new System.Collections.Stack();
            areas = new List<Area>();
            string directions = input[0];

            (int, int) currentPosition = (0, 0);
            areas.Add(new Area() { Type = Area.AreaType.Room, X = currentPosition.Item1, Y = currentPosition.Item2 });

            SetAreaIteration(directions, currentPosition);

            if (IsTest)
            {
                DrawArea(areas);
            }

            FindShortestPath(distances);
        }

        private void FindShortestPath(List<Area> distances)
        {
            Queue<Area> openList = new Queue<Area>();

            var initialRoom = areas.Where(a => a.Position == (0, 0)).First();
            initialRoom.Distance = 0;
            distances.Add(initialRoom);
            openList.Enqueue(initialRoom);
            Area currentRoom = null;

            while (openList.Count > 0)
            {
                currentRoom = openList.Dequeue();

                var doorAbove = areas.Where(a => a.Type == Area.AreaType.DoorNS).Where(a => a.Position == currentRoom.Above).FirstOrDefault();
                var doorLeft = areas.Where(a => a.Type == Area.AreaType.DoorEW).Where(a => a.Position == currentRoom.Left).FirstOrDefault();
                var doorRight = areas.Where(a => a.Type == Area.AreaType.DoorEW).Where(a => a.Position == currentRoom.Right).FirstOrDefault();
                var doorBelow = areas.Where(a => a.Type == Area.AreaType.DoorNS).Where(a => a.Position == currentRoom.Below).FirstOrDefault();

                if (doorAbove != null)
                {
                    var roomAbove = areas.First(a => a.Position == doorAbove.Above);
                    roomAbove.Parent = currentRoom;
                    int tmpDistance = currentRoom.Distance + 1;
                    if (distances.Any(d => d.Position == roomAbove.Position))
                    {
                        var a = distances.First(d => d.Position == roomAbove.Position);
                        if (tmpDistance < a.Distance)
                        {
                            a.Distance = tmpDistance;
                            openList.Enqueue(roomAbove);
                        }
                    }
                    else
                    {
                        roomAbove.Distance = tmpDistance;
                        distances.Add(roomAbove);
                        openList.Enqueue(roomAbove);
                    }
                }

                if (doorLeft != null)
                {
                    var roomLeft = areas.First(a => a.Position == doorLeft.Left);
                    roomLeft.Parent = currentRoom;
                    int tmpDistance = currentRoom.Distance + 1;
                    if (distances.Any(d => d.Position == roomLeft.Position))
                    {
                        var a = distances.First(d => d.Position == roomLeft.Position);
                        if (tmpDistance < a.Distance)
                        {
                            a.Distance = tmpDistance;
                            openList.Enqueue(roomLeft);
                        }
                    }
                    else
                    {
                        roomLeft.Distance = tmpDistance;
                        distances.Add(roomLeft);
                        openList.Enqueue(roomLeft);
                    }
                }

                if (doorRight != null)
                {
                    var roomRight = areas.First(a => a.Position == doorRight.Right);
                    roomRight.Parent = currentRoom;
                    int tmpDistance = currentRoom.Distance + 1;
                    if (distances.Any(d => d.Position == roomRight.Position))
                    {
                        var a = distances.First(d => d.Position == roomRight.Position);
                        if (tmpDistance < a.Distance)
                        {
                            a.Distance = tmpDistance;
                            openList.Enqueue(roomRight);
                        }
                    }
                    else
                    {
                        roomRight.Distance = tmpDistance;
                        distances.Add(roomRight);
                        openList.Enqueue(roomRight);
                    }
                }

                if (doorBelow != null)
                {
                    var roomBelow = areas.First(a => a.Position == doorBelow.Below);
                    roomBelow.Parent = currentRoom;
                    int tmpDistance = currentRoom.Distance + 1;
                    if (distances.Any(d => d.Position == roomBelow.Position))
                    {
                        var a = distances.First(d => d.Position == roomBelow.Position);
                        if (tmpDistance < a.Distance)
                        {
                            a.Distance = tmpDistance;
                            openList.Enqueue(roomBelow);
                        }
                    }
                    else
                    {
                        roomBelow.Distance = tmpDistance;
                        distances.Add(roomBelow);
                        openList.Enqueue(roomBelow);
                    }
                }
            }
        }

        private void SetAreaIteration(string directions, (int, int) currentPosition)
        {
            for (int i = 1; i < directions.Length; i++)
            {
                if (directions[i] == '$')
                {
                    //we reached the end of the input
                    return;
                }
                else if (directions[i] == '(')
                {
                    //more options!
                    //store current location - because you will need to start each of the groups on this location
                    Area area = areas.Where(a => a.Position == currentPosition).First();
                    area.HasMultipleOptions = true;
                    myStack.Push(area.Position);
                }
                else if (directions[i] == '|')
                {
                    //stop this path - break?
                    //go back to last stored location
                    currentPosition = ((int, int))myStack.Peek();
                }
                else if (directions[i] == ')')
                {
                    //stop this path - break?
                    //go back to last stored location
                    currentPosition = ((int, int))myStack.Pop();

                    if (directions[i - 1] == '|')
                    {
                        //we have an empty group
                        //go back to last stored location and set IsEmptyGroup to true!
                        Area area = areas.Where(a => a.Position == currentPosition).First();
                        area.ContainsEmptyOptions = true;
                        area.GroupId = i;
                    }
                }
                else
                {
                    var currentArea = areas.Where(a => a.Position == currentPosition).First();

                    switch (directions[i])
                    {
                        case 'N':
                            currentPosition = (currentPosition.Item1, currentPosition.Item2 - 1);
                            if (!areas.Any(a => a.Position == currentPosition))
                            {
                                areas.Add(new Area() { Type = Area.AreaType.DoorNS, X = currentPosition.Item1, Y = currentPosition.Item2 });
                            }
                            currentPosition = (currentPosition.Item1, currentPosition.Item2 - 1);
                            if (!areas.Any(a => a.Position == currentPosition))
                            {
                                areas.Add(new Area() { Type = Area.AreaType.Room, X = currentPosition.Item1, Y = currentPosition.Item2 });
                            }
                            break;
                        case 'W':
                            currentPosition = (currentPosition.Item1 - 1, currentPosition.Item2);
                            if (!areas.Any(a => a.Position == currentPosition))
                            {
                                areas.Add(new Area() { Type = Area.AreaType.DoorEW, X = currentPosition.Item1, Y = currentPosition.Item2 });
                            }
                            currentPosition = (currentPosition.Item1 - 1, currentPosition.Item2);
                            if (!areas.Any(a => a.Position == currentPosition))
                            {
                                areas.Add(new Area() { Type = Area.AreaType.Room, X = currentPosition.Item1, Y = currentPosition.Item2 });
                            }
                            break;
                        case 'S':
                            currentPosition = (currentPosition.Item1, currentPosition.Item2 + 1);
                            if (!areas.Any(a => a.Position == currentPosition))
                            {
                                areas.Add(new Area() { Type = Area.AreaType.DoorNS, X = currentPosition.Item1, Y = currentPosition.Item2 });
                            }
                            currentPosition = (currentPosition.Item1, currentPosition.Item2 + 1);
                            if (!areas.Any(a => a.Position == currentPosition))
                            {
                                areas.Add(new Area() { Type = Area.AreaType.Room, X = currentPosition.Item1, Y = currentPosition.Item2 });
                            }
                            break;
                        case 'E':
                            currentPosition = (currentPosition.Item1 + 1, currentPosition.Item2);
                            if (!areas.Any(a => a.Position == currentPosition))
                            {
                                areas.Add(new Area() { Type = Area.AreaType.DoorEW, X = currentPosition.Item1, Y = currentPosition.Item2 });
                            }
                            currentPosition = (currentPosition.Item1 + 1, currentPosition.Item2);
                            if (!areas.Any(a => a.Position == currentPosition))
                            {
                                areas.Add(new Area() { Type = Area.AreaType.Room, X = currentPosition.Item1, Y = currentPosition.Item2 });
                            }
                            break;
                    }

                    if (currentArea.ContainsEmptyOptions)
                    {
                        var newArea = areas.First(a => a.Position == currentPosition);
                        //we just came out of definition of empty group|)
                        //we have to mark next solution as only good one
                        newArea.IsOnlyGoodPathFromEmptyGroups = true;
                        newArea.GroupId = currentArea.GroupId;
                    }
                }
            }
        }

        private void DrawArea(List<Area> areas)
        {
            int minX = areas.Min(x => x.X);
            int maxX = areas.Max(x => x.X);
            int minY = areas.Min(x => x.Y);
            int maxY = areas.Max(x => x.Y);

            for (int j = minY - 1; j <= maxY + 1; j++)
            {
                for (int i = minX - 1; i <= maxX + 1; i++)
                {
                    if (i == 0 && j == 0)
                    {
                        Console.Write("X");
                    }
                    else if (areas.Any(x => x.X == i && x.Y == j))
                    {
                        var area = areas.Where(x => x.X == i && x.Y == j).First();

                        if (area.ContainsEmptyOptions)
                        {
                            Console.Write("C");
                        }
                        else if (area.IsOnlyGoodPathFromEmptyGroups)
                        {
                            Console.Write("G");
                        }
                        else
                        {
                            Console.Write(area.Type);
                        }
                    }
                    else
                    {
                        Console.Write("#");
                    }
                }

                Console.WriteLine();
            }
        }
    }

    public class Area
    {
        public class AreaType
        {
            public static string Wall = "#";
            public static string Room = ".";
            public static string DoorEW = "|";
            public static string DoorNS = "-";
        }

        public string Type { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public bool HasMultipleOptions { get; set; }
        public bool ContainsEmptyOptions { get; set; }
        public bool IsOnlyGoodPathFromEmptyGroups { get; set; }
        public int GroupId { get; set; }
        public (int, int) Position { get { return (X, Y); } }
        public (int, int) Above { get { return (X, Y - 1); } }
        public (int, int) Left { get { return (X - 1, Y); } }
        public (int, int) Right { get { return (X + 1, Y); } }
        public (int, int) Below { get { return (X, Y + 1); } }
        public int Distance { get; set; }
        public Area Parent { get; set; }
    }
}