﻿using System;
using System.Linq;
using System.Collections.Generic;
using Common;

namespace Day08
{
    class Program : BaseAoc
    {
        static void Main(string[] args)
        {
            Program program = new Program();
            program.SampleInputPartOne.Add(@"2 3 0 3 10 11 12 1 1 0 1 99 2 1 1 2", "138");

            program.SampleInputPartTwo.Add(@"2 3 0 3 10 11 12 1 1 0 1 99 2 1 1 2", "66");

            program.Solve();
        }

        protected override string FindFirstSolution(string[] input)
        {
            string[] data = input[0].Split(" ");
            int i = 0;
            Node node = SetNodeData(data, ref i);

            int sum = GetMetadataSum(node, 0);

            return sum.ToString();
        }

        protected override string FindSecondSolution(string[] input)
        {
            string[] data = input[0].Split(" ");
            int i = 0;
            Node node = SetNodeData(data, ref i);

            int sum = 0;
            GetPart2Sum(node, ref sum);

            return sum.ToString();
        }

        private int GetMetadataSum(Node node, int sum)
        {
            foreach (var child in node.Children)
            {
                sum = GetMetadataSum(child, sum);
            }

            sum += node.SumMetadata;

            return sum;
        }

        private Node SetNodeData(string[] data, ref int i)
        {
            Node node = new Node()
            {
                Position = i,
                NoChildren = int.Parse(data[i++].ToString()),
                NoMetadata = int.Parse(data[i++].ToString()),
            };
            for (int j = 0; j < node.NoChildren; j++)
            {
                node.Children.Add(SetNodeData(data, ref i));
            }

            List<int> meta = new List<int>();
            for (int j = 0; j < node.NoMetadata; j++)
            {
                meta.Add(int.Parse(data[i++].ToString()));
            }
            node.Metadata = meta;

            return node;
        }

        private void GetPart2Sum(Node node, ref int sum)
        {
            if (node.NoChildren == 0)
            {
                sum += node.SumMetadata;
            }
            else
            {
                foreach (var index in node.Metadata)
                {
                    if (index > node.NoChildren)
                    {
                        continue;
                    }

                    GetPart2Sum(node.Children[index - 1], ref sum);
                }
            }
        }
    }

    public class Node
    {
        public int Position { get; set; }
        public int NoChildren { get; set; }
        public List<Node> _children;
        public List<Node> Children
        {
            get
            {
                if (_children == null)
                {
                    _children = new List<Node>();
                }
                return _children;
            }
            set
            {
                _children = value;
            }
        }
        public int NoMetadata { get; set; }
        public List<int> _metadata;
        public List<int> Metadata
        {
            get
            {
                if (_metadata == null)
                {
                    _metadata = new List<int>();
                }
                return _metadata;
            }
            set
            {
                _metadata = value;
            }
        }

        public int SumMetadata { get { return Metadata.Sum(x => x); } }
    }
}