﻿using System;
using System.Linq;
using System.Collections.Generic;
using Common;

namespace Day10
{
    class Program : BaseAoc
    {
        static void Main(string[] args)
        {
            Program program = new Program();

            program.Solve();
        }

        protected override string FindFirstSolution(string[] input)
        {
            //prepare data
            List<Star> stars = ParseData(input);
            (List<Star> alignedStars, int minX, int maxX, int minY, int maxY, int counter) = FindProperCanvasSize(stars);

            minX -= 5;
            maxX += 5;
            minY -= 5;
            maxY += 5;

            for (int j = minY; j < maxY; j++)
            {
                for (int i = minX; i < maxX; i++)
                {
                    Console.Write(alignedStars.Any(x => (x.PositionX == i && x.PositionY == j)) ? "#" : " ");
                }

                Console.WriteLine("");
            }

            return "AHZLLCAL";
        }

        protected override string FindSecondSolution(string[] input)
        {
            //prepare data
            List<Star> stars = ParseData(input);
            (List<Star> alignedStars, int minX, int maxX, int minY, int maxY, int counter) = FindProperCanvasSize(stars);

            return counter.ToString();
        }

        private (List<Star> stars, int minX, int maxX, int minY, int maxY, int counter) FindProperCanvasSize(List<Star> stars)
        {
            //find proper canvas size
            int minX = stars.Min(x => x.NextPositionX);
            int maxX = stars.Max(x => x.NextPositionX);
            int minY = stars.Min(x => x.NextPositionY);
            int maxY = stars.Max(x => x.NextPositionY);
            int counter = 0;
            while (true)
            {
                counter++;
                stars = MoveStars(stars);
                if (stars.Min(x => x.NextPositionX < minX) || stars.Max(x => x.NextPositionX > maxX) || stars.Min(x => x.NextPositionY < minY) || stars.Max(x => x.NextPositionY > maxY))
                {
                    break;
                }
                minX = stars.Min(x => x.NextPositionX);
                maxX = stars.Max(x => x.NextPositionX);
                minY = stars.Min(x => x.NextPositionY);
                maxY = stars.Max(x => x.NextPositionY);
            }

            return (stars, minX, maxX, minY, maxY, counter);
        }

        private List<Star> MoveStars(List<Star> stars)
        {
            foreach (Star star in stars)
            {
                star.MoveItMoveItMoveIt();
            }

            return stars;
        }

        private static List<Star> ParseData(string[] input)
        {
            List<Star> stars = new List<Star>();
            foreach (string line in input)
            {
                string[] data = line.Replace("position=<", "").Replace("velocity=<", ",").Replace(">", "").Replace(" ", "").Replace(" ", "").Split(",");
                stars.Add(new Star()
                {
                    PositionX = int.Parse(data[0]),
                    PositionY = int.Parse(data[1]),
                    MovementX = int.Parse(data[2]),
                    MovementY = int.Parse(data[3])
                });
            }

            return stars;
        }
    }

    public class Star
    {
        public int PositionX { get; set; }
        public int PositionY { get; set; }
        public int MovementX { get; set; }
        public int MovementY { get; set; }

        public int NextPositionX
        {
            get
            {
                return PositionX + MovementX;
            }
        }
        public int NextPositionY
        {
            get
            {
                return PositionY + MovementY;
            }
        }

        public void MoveItMoveItMoveIt()
        {
            PositionX += MovementX;
            PositionY += MovementY;
        }
    }
}