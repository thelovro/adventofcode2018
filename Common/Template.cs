﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Common
{
    class Program : BaseAoc
    {
        static void Main(string[] args)
        {
            Program program = new Program();
            program.SampleInputPartOne.Add(@"", "");

            program.Solve();
        }

        protected override string FindFirstSolution(string[] input)
        {
            return "";
        }

        protected override string FindSecondSolution(string[] input)
        {
            return "";
        }
    }
}