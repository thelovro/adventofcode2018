﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Collections.Generic;
using Common;

namespace Day04
{
    class Program : BaseAoc
    {
        static void Main(string[] args)
        {
            Program program = new Program();
            program.SampleInputPartOne.Add(@"[1518-11-01 00:00] Guard #10 begins shift
[1518-11-01 00:05] falls asleep
[1518-11-01 00:25] wakes up
[1518-11-01 00:30] falls asleep
[1518-11-01 00:55] wakes up
[1518-11-01 23:58] Guard #99 begins shift
[1518-11-02 00:40] falls asleep
[1518-11-02 00:50] wakes up
[1518-11-03 00:05] Guard #10 begins shift
[1518-11-03 00:24] falls asleep
[1518-11-03 00:29] wakes up
[1518-11-04 00:02] Guard #99 begins shift
[1518-11-04 00:36] falls asleep
[1518-11-04 00:46] wakes up
[1518-11-05 00:03] Guard #99 begins shift
[1518-11-05 00:45] falls asleep
[1518-11-05 00:55] wakes up", "240");

            program.Solve();
        }

        protected override string FindFirstSolution(string[] input)
        {
            List<Row> parsedInput = ParseInput(input);
            int solution = FindGuard(parsedInput);

            return solution.ToString();
        }

        protected override string FindSecondSolution(string[] input)
        {
            List<Row> parsedInput = ParseInput(input);
            int solution = FindGuard2(parsedInput);

            return solution.ToString();
        }

        private static int FindGuard(List<Row> input)
        {
            var guards = input.GroupBy(x => x.GuardId).Select(x => new { GuardId = x.Key, Minutes = x.Sum(s => s.SumMinutesAsleep) });

            var guardWithMostMinutesAsleep = guards.OrderBy(x => x.Minutes).Select(x => x.GuardId).Last();

            int[] _minutesAsleep = new int[60];
            for (int i = 0; i < 60; i++)
            {
                _minutesAsleep[i] = 0;
            }
            foreach (var item in input.Where(x => x.GuardId == guardWithMostMinutesAsleep))
            {
                for (int i = 0; i < 60; i++)
                {
                    _minutesAsleep[i] += item.MinutesAsleep[i];
                }
            }

            int maxId = 0;
            int maxValue = 0;
            for (int i = 0; i < 60; i++)
            {
                if (_minutesAsleep[i] > maxValue)
                {
                    maxValue = _minutesAsleep[i];
                    maxId = i;
                }
            }

            return guardWithMostMinutesAsleep * maxId;
        }

        private static int FindGuard2(List<Row> input)
        {
            var guards = input.GroupBy(x => x.GuardId).Select(x => x.Key);

            List<GuardRow> guardRows = new List<GuardRow>();
            foreach (var guard in guards)
            {
                GuardRow guardRow = new GuardRow();
                guardRow.GuardId = guard;

                foreach (var item in input.Where(x => x.GuardId == guard))
                {
                    for (int i = 0; i < 60; i++)
                    {
                        guardRow.MinutesAsleep[i] += item.MinutesAsleep[i];
                    }
                }

                guardRows.Add(guardRow);
            }

            var guardWithMostMinutesAsleep = guardRows.OrderBy(x => x.MaxValue).Select(x => new { x.GuardId, x.MaxMinute }).Last();

            return guardWithMostMinutesAsleep.GuardId * guardWithMostMinutesAsleep.MaxMinute;
        }

        private static List<Row> ParseInput(string[] input)
        {
            Array.Sort(input);
            List<Row> parsedRows = new List<Row>();

            int i = 0;
            while (i < input.Length)
            {
                if (input[i].Split(" ")[2] == "Guard")
                {
                    if (input[i + 1].Split(" ")[2] == "Guard")
                    {
                        i++;
                        continue;
                    }
                }

                string[] guard = input[i].Split(" ");
                Row guardRow = new Row();
                guardRow.Date = guard[0].TrimStart('[');
                guardRow.Time = guard[1].TrimEnd(']');
                guardRow.GuardId = int.Parse(guard[3].TrimStart('#'));

                i++;

                while (i < input.Length && input[i].Split(" ")[2] != "Guard")
                {
                    string[] start = input[i++].Split(" ");
                    string[] end = input[i++].Split(" ");
                    int startMinute = int.Parse(start[1].TrimEnd(']').Split(":")[1]);
                    int endMinute = int.Parse(end[1].TrimEnd(']').Split(":")[1]);

                    for (int j = startMinute; j < endMinute; j++)
                    {
                        guardRow.MinutesAsleep[j] = 1;
                    }
                }

                parsedRows.Add(guardRow);
            }

            return parsedRows;
        }
    }

    public class Row
    {
        public string Date { get; set; }
        public string Time { get; set; }

        public int GuardId { get; set; }
        int[] _minutesAsleep = null;
        public int[] MinutesAsleep
        {
            get
            {
                if (_minutesAsleep == null)
                {
                    _minutesAsleep = new int[60];
                    for (int i = 0; i < 60; i++)
                    {
                        _minutesAsleep[i] = 0;
                    }
                }
                return _minutesAsleep;
            }
            set
            {
                _minutesAsleep = value;
            }
        }

        public int SumMinutesAsleep
        {
            get
            {
                return MinutesAsleep.Sum();
            }
        }
    }

    public class GuardRow
    {
        public int GuardId { get; set; }
        int[] _minutesAsleep = null;
        public int[] MinutesAsleep
        {
            get
            {
                if (_minutesAsleep == null)
                {
                    _minutesAsleep = new int[60];
                    for (int i = 0; i < 60; i++)
                    {
                        _minutesAsleep[i] = 0;
                    }
                }
                return _minutesAsleep;
            }
            set
            {
                _minutesAsleep = value;
            }
        }

        public int MaxMinute
        {
            get
            {
                int maxId = 0;
                int maxValue = 0;
                for (int i = 0; i < 60; i++)
                {
                    if (_minutesAsleep[i] > maxValue)
                    {
                        maxValue = _minutesAsleep[i];
                        maxId = i;
                    }
                }

                return maxId;
            }
        }

        public int MaxValue
        {
            get
            {
                int maxId = 0;
                int maxValue = 0;
                for (int i = 0; i < 60; i++)
                {
                    if (_minutesAsleep[i] > maxValue)
                    {
                        maxValue = _minutesAsleep[i];
                        maxId = i;
                    }
                }

                return maxValue;
            }
        }
    }
}